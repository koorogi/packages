# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=tellico
pkgver=3.1.3
pkgrel=1
pkgdesc="Collection manager"
url="http://tellico-project.org/"
arch="all"
license="GPL-2.0-only OR GPL-3.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev libxml2-dev libxslt-dev
	karchive-dev kcodecs-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev
	kcrash-dev kdoctools-dev kguiaddons-dev khtml-dev ki18n-dev
	kiconthemes-dev kio-dev kitemmodels-dev kjobwidgets-dev kwallet-dev
	kwidgetsaddons-dev kwindowsystem-dev kxmlgui-dev solid-dev

	kfilemetadata-dev knewstuff-dev libcdio-dev libksane-dev ncurses-dev
	poppler-dev poppler-qt5-dev taglib-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="http://tellico-project.org/files/tellico-$pkgver.tar.xz
	btparse-strcasecmp.patch
	"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS -std=gnu99" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	# imagejob: needs running X11
	# htmlexporter: needs plasma desktop
	# filelisting: needs dbus
	# tellicoread: needs network
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E '(filelisting|imagejob|htmlexporter|tellicoread)test'
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="9024f423f0685e834ed46e7038c5b95fab5b684aaac00d6298e23493fb4290daeda994faf36a0f04973093a477be5506591e6b0e7f57ef5591fb10953ad8ec5e  tellico-3.1.3.tar.xz
4627e717d67340de6d88f7a21604a66ba236c651a0ae38d9d3569b76ad58c79f046cfd5686dd688de86d6acafc17ba3959902babdc7f00ab8e9d65717c4fab4a  btparse-strcasecmp.patch"
