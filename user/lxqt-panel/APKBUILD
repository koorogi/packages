# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=lxqt-panel
pkgver=0.14.0
pkgrel=0
pkgdesc="Panel for LXQt desktop"
url="https://lxqt.org"
arch="all"
license="LGPL-2.1+"
depends=""
makedepends="cmake extra-cmake-modules lxqt-build-tools>=0.6.0 kguiaddons-dev
	libdbusmenu-qt-dev kwindowsystem-dev solid-dev menu-cache-dev
	lxmenu-data liblxqt-dev>=${pkgver%.*}.0 alsa-lib-dev pulseaudio-dev
	lxqt-globalkeys-dev>=${pkgver%.*}.0 lm_sensors-dev libstatgrab-dev
	libsysstat-dev qt5-qttools-dev libxkbcommon-dev libxcomposite-dev"
subpackages="$pkgname-doc"
source="https://github.com/lxqt/lxqt-panel/releases/download/$pkgver/lxqt-panel-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	mkdir -p build && cd build
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} ..
	make
}

check() {
	cd "$builddir"/build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install
}

sha512sums="771383c2bade7f5928d2a206b7a148afd7427a574e875c3129135c8cc4b08a77f8ab1ce469e1b1ee1902d984c17605b62f2f7aacac2ca415c9021b314d18dfe3  lxqt-panel-0.14.0.tar.xz"
