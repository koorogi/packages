# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kgoldrunner
pkgver=18.12.2
pkgrel=0
pkgdesc="Puzzle game with a gold hunt, dodging enemies, and digging around"
url="https://games.kde.org/game.php?game=kgoldrunner"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	qt5-qtsvg-dev kcoreaddons-dev kconfig-dev kconfigwidgets-dev kcrash-dev
	kdbusaddons-dev kdoctools-dev ki18n-dev kio-dev kxmlgui-dev phonon-dev
	libkdegames-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kgoldrunner-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="4d918dbb02675954dd9854c4bd37a84356921235026f38c3c4abf0f7326b162b6ef59543b8b20c2ea11877000c3ab1cc9984aab006db95f67e41dd435f0eab28  kgoldrunner-18.12.2.tar.xz"
