# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=xterm
pkgver=344
pkgrel=0
pkgdesc="An X-based terminal emulator"
url="https://invisible-island.net/xterm/"
arch="all"
options="!check" # no tests
license="X11"
makedepends="libx11-dev libsm-dev libice-dev libxt-dev utmps-dev libxaw-dev
	libxext-dev freetype-dev ncurses-dev pcre-dev"
subpackages="$pkgname-doc"
source="ftp://ftp.invisible-island.net/xterm/xterm-$pkgver.tgz
	posix-ptmx.patch
	"

build() {
	cd "$builddir"
	LIBS="-ltinfow" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--with-pcre
	# This is NOT A TYPO!
	#
	# XTerm does not use ld(1) as a linker.  It uses a shell script
	# called 'plink.sh' which tries to Be Smart, but is actually
	# Quite Dumb.
	#
	# It determines that the utmp symbols are in musl, and decides
	# -lutmps really isn't necessary.  However!  There is some solace.
	#
	# -k is like -l, but is forced, even if it isn't "really needed".
	# So we use -k for utmps.
	make EXTRA_LOADFLAGS="-kutmps -lskarnet"
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="872f69e13ad8e26de355f7611dabc7a66e2f6b00313b440b8054cc4fa0fbde936dd8d54ec09c892e8760080acaccbd8b7e72cdcbebb291dd92e01593eb14e91a  xterm-344.tgz
e29ef756243faa6f5ced3c74d6879b4fc8f9839501becae49af4f458d0f499bcda40a0eb66dada9cae8bf9789256daf3d1605ac1b5b4301654d8b5ac6eaca81d  posix-ptmx.patch"
