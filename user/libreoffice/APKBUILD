# Contributor: Jens Staal <staal1978@gmail.com>
# Contributor: Timo Teräs <timo.teras@iki.fi>
# Maintainer: Max Rees <maxcrees@me.com>

# You probably don't want "srcdir", "pkgdir", or "deps" in CLEANUP,
# and you definitely don't want "deps" in ERROR_CLEANUP.
# Just "abuild clean undeps" once you're completely done.
#
# Build requirements:
# * RAM:
#   8 GB with Hulu in Chromium in the background is enough.
#
# * Disk space:
#   * Dependencies:  1216 MiB
#   * Downloads:      372 MiB
#   * Subtotal:      1588 MiB =  1.6 GiB
#   * Complete src: 28682 MiB
#   * Complete pkg:  7935 MiB
#   * All APKs:      1934 MiB =  1.9 GiB
#   * Subtotal:     38551 MiB = 37.6 GiB
#   * Grand total:  40139 MiB = 39.2 GiB
#
# Build stats:
# * Run "abuild deps fetch" first.
# * time abuild -r
#
# * x86_64 Intel i7-4810MQ (4 core 2.8 GHz, turbo to 3.8 GHz, no HT)
#   JOBS=4, 8 GB RAM
#   abuild -r  29590.16s user 1077.69s system 340% cpu 2:30:13.12 total

pkgname=libreoffice
pkgver=6.0.6.2
pkgrel=1
case "$pkgver" in
*.*.*.*) _ver="${pkgver%.*}";;
*.*.*) _ver="$pkgver";;
esac
pkgdesc="LibreOffice - Meta package for the full office suite"
url="http://www.libreoffice.org/"
# While the metapackage technically is empty and should be "noarch", there's
# no easy way to change this to noarch and then change all of the subpackages
# to have the correct arch. Setting $arch in a split function is forbidden,
# and $subpackages doesn't really support setting $arch to anything other than
# noarch.
arch="all"
options="!checkroot"
license="MPL-2.0 AND Apache-2.0 AND MIT AND X11 AND (MPL-1.1 OR GPL-2.0+ OR LGPL-2.1+) AND GPL-2.0+ AND GPL-3.0 AND GPL-3.0+ AND LGPL-2.1 AND LGPL-3.0+ AND BSD-3-Clause AND SISSL AND IJG AND CC-BY-SA-3.0"

depends="$pkgname-base $pkgname-calc $pkgname-common $pkgname-draw
	$pkgname-impress $pkgname-math $pkgname-connector-postgres $pkgname-writer"
# 1. Base dependencies
# 2. GUIs - gen
# 3. GUIs - gtk+2.0
# 4. File formats
makedepends="apr-dev bash bison boost-dev cairo-dev clucene-dev cmd:which
	coreutils cppunit-dev cups-dev dbus-glib-dev findutils flex
	fontconfig-dev freetype-dev gettext-tiny-dev glm gperf gpgme-dev
	gst-plugins-base-dev gstreamer-dev harfbuzz-dev hunspell-dev
	hyphen-dev icu icu-dev lcms2-dev libcmis-dev libexttextcat-dev
	libjpeg-turbo-dev libpng-dev libxml2-utils libxslt-dev mdds~1.3
	mythes-dev neon-dev nss-dev openldap-dev openssl-dev paxmark perl
	poppler-dev postgresql-dev python3-dev redland-dev sane-dev sed ucpp
	unixodbc-dev util-linux xmlsec-dev zip

	libepoxy-dev libxinerama-dev libxrandr-dev libxrender-dev libxext-dev

	gtk+2.0-dev gdk-pixbuf-dev glib-dev

	libabw-dev libcdr-dev libe-book-dev libepubgen-dev libetonyek-dev
	libfreehand-dev libmspub-dev libmwaw-dev libodfgen-dev liborcus-dev~0.13
	libpagemaker-dev libqxp-dev libstaroffice-dev libvisio-dev libwpd-dev
	libwpg-dev libwps-dev libzmf-dev
"

# -common also depends on these fonts
_fonts="ttf-liberation ttf-dejavu ttf-carlito"
checkdepends="$_fonts"

# The order here is important.
# -doc    comes first since it redirects manpages from the other subpackages
# -lang-* comes before -common since it redirects miscellaneous
#         language-specific files from -common
subpackages="$pkgname-doc $pkgname-base $pkgname-gtk2
	$pkgname-calc $pkgname-draw $pkgname-impress $pkgname-math
	$pkgname-connector-postgres $pkgname-writer"
source="https://download.documentfoundation.org/$pkgname/src/$_ver/$pkgname-$pkgver.tar.xz
	https://download.documentfoundation.org/$pkgname/src/$_ver/$pkgname-dictionaries-$pkgver.tar.xz
	https://download.documentfoundation.org/$pkgname/src/$_ver/$pkgname-translations-$pkgver.tar.xz
	https://download.documentfoundation.org/$pkgname/src/$_ver/$pkgname-help-$pkgver.tar.xz
	linux-musl.patch
	fix-execinfo.patch
	fix-includes.patch
	gettext-tiny.patch
	disable-crc-test.patch
	disable-outdated-font-test.patch
"
ldpath="/usr/lib/$pkgname/program"

_languages="af:Afrikaans:MPL-2.0 AND Public-Domain AND LGPL-3.0 AND LGPL-2.1+ AND Apache-2.0
	am:Amharic:MPL-2.0 AND Apache-2.0
	an:Aragonese:MPL-1.1 OR GPL-3.0+ OR LGPL-3.0+
	ar:Arabic:MPL-2.0 AND (GPL-2.0+ OR LGPL-2.1+ OR MPL-1.1+) AND Apache-2.0
	as:Assamese:MPL-2.0 AND Apache-2.0
	ast:Asturian:MPL-2.0 AND Apache-2.0
	be:Belarusian:MPL-2.0 AND CC-BY-SA-3.0 AND Apache-2.0
	bg:Bulgarian:MPL-2.0 AND GPL-2.0+ AND Apache-2.0
	bn:Bengali:MPL-2.0 AND GPL-2.0 AND Apache-2.0
	bn_in:Bengali (India):MPL-2.0 AND Apache-2.0
	bo:Tibetan:MPL-2.0 AND Apache-2.0
	br:Breton:MPL-2.0 AND LGPL-3.0 AND Apache-2.0
	brx:Bodo:MPL-2.0 AND Apache-2.0
	bs:Bosnian:MPL-2.0 AND Apache-2.0
	ca:Catalan:MPL-2.0 AND GPL-2.0+ AND GPL-3.0+ AND (GPL-3.0+ OR LGPL-3.0+) AND Apache-2.0
	ca_valencia:Catalan (Valencian):MPL-2.0 AND Apache-2.0
	cs:Czech:MPL-2.0 AND Custom AND Apache-2.0
	cy:Welsh (Cymraeg):MPL-2.0 AND Apache-2.0
	da:Danish:MPL-2.0 AND (GPL-2.0 OR LGPL-2.1 OR MPL-1.1) AND Apache-2.0
	de:German:MPL-2.0 AND (GPL-2.0 OR GPL-3.0 OR OASIS-0.1) AND Apache-2.0
	dgo:Dogri proper:MPL-2.0 AND Apache-2.0
	dz:Dzongkha:MPL-2.0 AND Apache-2.0
	el:Greek:MPL-2.0 AND (GPL-2.0 OR LGPL-2.1 OR MPL-1.1) AND Apache-2.0
	en_gb:English (UK):MPL-2.0
	en_us:English (US):MPL-2.0 AND Custom AND MIT AND (MPL-1.1 OR GPL-3.0+ OR LGPL-3.0+) AND GPL-2.0+ AND LGPL-2.1+ AND Apache-2.0
	en_za:English (South Africa):MPL-2.0
	eo:Esperanto:MPL-2.0 AND Apache-2.0
	es:Spanish:MPL-2.0 AND (GPL-3.0 OR LGPL-3.0 OR MPL-1.1) AND LGPL-2.1 AND Apache-2.0
	et:Estonian:MPL-2.0 AND LGPL-2.1 AND LPPL-1.3c AND Apache-2.0
	eu:Basque:MPL-2.0 AND Apache-2.0
	fa:Persian (Farsi):MPL-2.0 AND Apache-2.0
	fi:Finnish:MPL-2.0 AND Apache-2.0
	fr:French:MPL-2.0 AND (MPL-1.1+ OR GPL-2.0+ OR LGPL-2.1+) AND LPPL-1.3c AND LGPL-2.1+ AND Apache-2.0
	ga:Irish:MPL-2.0 AND Apache-2.0
	gd:Scottish Gaelic:MPL-2.0 AND GPL-3.0 AND Apache-2.0
	gl:Galician:MPL-2.0 AND GPL-3.0 AND Apache-2.0
	gu:Gujarati:MPL-2.0 AND GPL-3.0 AND Apache-2.0
	gug:Guaraní (Paraguay):MPL-2.0 AND Apache-2.0
	he:Hebrew:MPL-2.0 AND GPL-3.0 AND Apache-2.0
	hi:Hindi:MPL-2.0 AND GPL-2.0+ AND Apache-2.0
	hr:Croatian:MPL-2.0 AND LGPL-2.1 AND Apache-2.0
	hu:Hungarian:MPL-2.0 AND (GPL-2.0+ OR LGPL-2.1+ OR MPL-1.1+) AND Apache-2.0
	id:Indonesian:MPL-2.0 AND Apache-2.0
	is:Icelandic:MPL-2.0 AND CC-BY-SA-3.0 AND Apache-2.0
	it:Italian:MPL-2.0 AND GPL-3.0 AND LGPL-3.0 AND Apache-2.0
	ja:Japanese:MPL-2.0 AND Apache-2.0
	ka:Georgian:MPL-2.0 AND Apache-2.0
	kk:Kazakh:MPL-2.0 AND Apache-2.0
	km:Khmer:MPL-2.0 AND Apache-2.0
	kmr_latn:Kurmanji Kurdish (Latin):MPL-2.0 AND Apache-2.0
	kn:Kannada:MPL-2.0 AND Apache-2.0
	ko:Korean:MPL-2.0 AND Apache-2.0
	kok:Konkani:MPL-2.0 AND Apache-2.0
	ks:Kashmiri:MPL-2.0 AND Apache-2.0
	lb:Luxembourgish:MPL-2.0 AND Apache-2.0
	lo:Lao:MPL-2.0 AND LGPL-2.1 AND Apache-2.0
	lt:Lithuanian:MPL-2.0 AND BSD-3-Clause AND LPPL-1.3c AND Apache-2.0
	lv:Latvian:MPL-2.0 AND LGPL-2.1 AND Apache-2.0
	mai:Maithili:MPL-2.0 AND Apache-2.0
	mk:Macedonian:MPL-2.0 AND Apache-2.0
	ml:Malayalam:MPL-2.0 AND Apache-2.0
	mn:Mongolian:MPL-2.0 AND Apache-2.0
	mni:Meithei (Manipuri):MPL-2.0 AND Apache-2.0
	mr:Marathi:MPL-2.0 AND Apache-2.0
	my:Burmese:MPL-2.0 AND Apache-2.0
	nb:Norwegian (Bokmal):MPL-2.0 AND Apache-2.0
	ne:Nepali:MPL-2.0 AND LGPL-2.1 AND Apache-2.0
	nl:Dutch:MPL-2.0 AND (BSD-2-Clause OR CC-BY-3.0) AND Apache-2.0
	no:Norwegian:GPL-2.0
	nn:Nynorsk:MPL-2.0 AND Apache-2.0
	nr:Ndebele (South):MPL-2.0 AND Apache-2.0
	nso:Northern Sotho:MPL-2.0 AND Apache-2.0
	oc:Occitan:MPL-2.0 AND GPL-2.0+ AND Apache-2.0
	om:Oromo:MPL-2.0 AND Apache-2.0
	or:Oriya:MPL-2.0 AND Apache-2.0
	pa_in:Punjabi (India):MPL-2.0 AND Apache-2.0
	pl:Polish:MPL-2.0 AND (GPL OR LGPL OR MPL OR CC-SA-1.0) AND LGPL-3.0 AND LGPL-2.1 AND Apache-2.0
	pt:Portuguese:MPL-2.0 AND (GPL-2.0 OR LGPL-2.1 OR MPL-1.1) AND GPL-2.0 AND Apache-2.0
	pt_br:Portuguese (Brazil):MPL-2.0 AND (LGPL-3.0 OR MPL-1.1) AND (GPL-3.0+ OR LGPL-3.0+ OR MPL-1.1) AND Apache-2.0
	ro:Romanian:MPL-2.0 AND (GPL-2.0 OR LGPL-2.1 OR MPL-1.1) AND GPL-2.0 AND GPL-2.0+ AND Apache-2.0
	ru:Russian:MPL-2.0 AND (MPL-1.1 OR GPL OR LGPL) AND Custom AND LGPL AND Apache-2.0
	rw:Kinyarwanda:MPL-2.0 AND Apache-2.0
	sa_in:Sanskrit (India):MPL-2.0 AND Apache-2.0
	sat:Santali:MPL-2.0 AND Apache-2.0
	sd:Sindhi:MPL-2.0 AND Apache-2.0
	si:Sinhala:MPL-2.0 AND GPL-3.0 AND Apache-2.0
	sid:Sidamo:MPL-2.0 AND Apache-2.0
	sk:Slovak:MPL-2.0 AND (GPL-2.0 OR LGPL-2.1 OR MPL-1.1) AND LPPL-1.3c AND MIT AND Apache-2.0
	sl:Slovenian:MPL-2.0 AND (GPL-2.0 OR LGPL-2.1) AND LGPL-2.1 AND Apache-2.0
	sq:Albanian:MPL-2.0 AND Apache-2.0
	sr:Serbian:MPL-2.0 AND (LGPL-2.1+ OR MPL-1.1+ OR GPL-2.0+ OR CC-BY-SA-3.0) AND LGPL-2.1+ AND Apache-2.0
	sr_latn:Serbian (Latin):MPL-2.0 AND Apache-2.0
	ss:Swati:MPL-2.0 AND Apache-2.0
	st:Southern Sotho:MPL-2.0 AND Apache-2.0
	sv:Swedish:MPL-2.0 AND LGPL-3.0 AND Custom AND Apache-2.0
	sw_tz:Swahili (Tanzania):MPL-2.0 AND LGPL-2.1 AND Apache-2.0
	ta:Tamil:MPL-2.0 AND Apache-2.0
	te:Telugu:MPL-2.0 AND GPL-2.0+ AND (GPL-3.0+ OR LGPL-3.0+) AND Apache-2.0
	tg:Tajik:MPL-2.0 AND Apache-2.0
	th:Thai:MPL-2.0 AND LGPL-2.1 AND Apache-2.0
	tn:Tswana:MPL-2.0 AND Apache-2.0
	tr:Turkish:MPL-2.0 AND Apache-2.0
	ts:Tsonga:MPL-2.0 AND Apache-2.0
	tt:Tatar:MPL-2.0 AND Apache-2.0
	ug:Uyghur:MPL-2.0 AND Apache-2.0
	uk:Ukrainian:MPL-2.0 AND (GPL-2.0+ OR LGPL-2.1+ OR MPL-1.1) AND GPL-2.0+ AND Apache-2.0
	uz:Uzbek:MPL-2.0 AND Apache-2.0
	ve:Venda:MPL-2.0 AND Apache-2.0
	vi:Vietnamese:MPL-2.0 AND GPL-2.0 AND Apache-2.0
	xh:Xhosa:MPL-2.0 AND Apache-2.0
	zh_cn:Simplified Chinese (People's Republic of China):MPL-2.0 AND Apache-2.0
	zh_tw:Traditional Chinese (Taiwan):MPL-2.0 AND Apache-2.0
	zu:Zulu:MPL-2.0 AND LGPL-2.1 AND Apache-2.0
"
_lo_lang() {
	local lang="$1"
	case "$lang" in
	# e.g. zh_cn -> zh_CN
	*_[a-z][a-z]) lang="${lang%_*}_$(printf '%s' "${lang#*_}" | tr '[a-z]' '[A-Z]')";;
	# e.g. sr_latn -> sr_Latn
	*_latn) lang="${lang%_latn}_Latn";;
	esac
	printf '%s' "$lang"
}
_lo_languages=""
for _lang in $(printf '%s' "$_languages" | cut -d : -f 1); do
	subpackages="$subpackages $pkgname-lang-$_lang:_split_lang:noarch"
	# Seriously now. We even have secret languages that are not recognized
	# by the configure script. These two languages only have dictionaries.
	# c.f. _split_lang()
	[ "$_lang" = "an" ] || [ "$_lang" = "no" ] && continue

	# --with-languages seems to prefer dashes instead of underscores
	# when --with-myspell-dicts is given
	_lang="$(_lo_lang "$_lang" | tr _ -)"
	_lo_languages="$_lo_languages $_lang"
done
subpackages="$subpackages $pkgname-common"

prepare() {
	cd "$builddir"

	default_prepare
	NOCONFIGURE=1 ./autogen.sh
}

build() {
	cd "$builddir"

	export PYTHON="python3"
	# Note: --with-parallelism must be specified since getconf does not
	#       recognize _NPROCESSORS_ONLN

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--with-vendor="Adélie Linux" \
		--enable-symbols \
		--with-parallelism="$JOBS" \
		--disable-online-update \
		--disable-fetch-external \
		--disable-dependency-tracking \
		--enable-release-build \
		--enable-split-app-modules \
		--enable-python=system \
		--with-alloc=system \
		--with-tls=nss \
		--with-system-libs \
		--with-system-ucpp \
		--with-help \
		--without-system-dicts \
		--with-external-tar="$srcdir" \
		--with-lang="$_lo_languages" \
		--with-myspell-dicts \
		--without-fonts \
		--disable-firebird-sdbc \
		--disable-coinmp \
		--disable-lpsolve \
		--disable-gtk3 \
		--enable-gtk \
		--disable-qt5 \
		--disable-odk \
		--disable-avahi \
		--disable-scripting-beanshell \
		--disable-scripting-javascript \
		--disable-sdremote \
		--disable-sdremote-bluetooth \
		--disable-pdfium \
		--disable-ooenv \
		--without-java \
		--disable-epm

	# adding '-isystem /usr/include' make things break with gcc6
	# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=823145
	sed -i -e 's:-isystem /usr/include[^/]::g' config_host.mk

	make build-nocheck
}

check() {
	cd "$builddir"
	make -k unitcheck
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" distro-pack-install
}

doc() {
	default_doc
	pkgdesc="LibreOffice - man pages"
	sed -i -e '\#^/usr/share/man#d' "$builddir"/file-lists/*.txt
}

_split() {
	local i
	for i in $(grep -v ^%dir "$builddir/file-lists/${1}_list.txt" | sort -u); do
		dirname="$(dirname $i)"
		[ -d "$subpkgdir/$dirname" ] || install -dm755 "$subpkgdir/$dirname"
		mv "$pkgdir/$i" "$subpkgdir/$i"
	done
}

_move_from() {
	orig_pkg="$1" # original owner of $path
	path="$2"     # file/directory to move
	dest="$3"     # destination directory, automatically under $subpkgdir

	sed -i -e "\\#^\\(%dir \\)*/${path#$pkgdir/}#d" \
		"$builddir/file-lists/${orig_pkg}_list.txt"
	[ -d "$subpkgdir/$dest" ] || install -dm755 "$subpkgdir/$dest"
	mv "$path" "$subpkgdir/$dest"
}

_split_lang() {
	local i lang entry dict dictdir auto autodir wiz wizdir logo logodir
	lang="${subpkgname#$pkgname-lang-}"
	entry="$(printf '%s' "$_languages" | grep "^\\s*$lang")"
	lang="$(_lo_lang "$lang")"

	pkgdesc="LibreOffice - $(printf '%s' "$entry" | cut -d : -f 2) language pack"
	license="$(printf '%s' "$entry" | cut -d : -f 3)"
	depends=""

	# Includes translations/messages and help packs initially
	_split "lang_$lang"
	# Everything else we must move by hand

	dictdir="usr/lib/libreoffice/share/extensions"
	case "$lang" in
	# en_US is installed by default, so it will own most of the English files
	en_US) dict="en";;
	pt) dict="pt-PT";;
	*) dict="$(printf '%s' "$lang" | tr _ -)";;
	esac
	if [ -d "$pkgdir/$dictdir/dict-$dict" ]; then
		_move_from common "$pkgdir/$dictdir/dict-$dict" "$dictdir"
	fi
	# Again, these languages only have dictionaries
	[ "$_lang" = "an" ] || [ "$_lang" = "no" ] && return 0

	autodir="usr/lib/libreoffice/share/autocorr"
	case "$lang" in
	de) auto="de";;
	en_US) auto="en-[A-Z][A-Z]";;
	en_*) auto="skip";;
	es) auto="es";;
	fr) auto="fr";;
	it) auto="it";;
	pt) auto="pt-PT";;
	pt_BR) auto="pt-BR";;
	*_[A-Z][A-Z]) auto="$(printf '%s' "$lang" | tr _ -)";;
	*_Latn) auto="${lang%_Latn}-Latn-[A-Z][A-Z]";;
	*) auto="$lang-[A-Z][A-Z]";;
	esac
	for i in $(find "$pkgdir/$autodir" -name "acor_$auto.dat"); do
		_move_from common "$i" "$autodir"
	done

	wizdir="usr/lib/libreoffice/share/wizards"
	case "$lang" in
	en_US) wiz="en_[A-Z][A-Z]";;
	en_*) wiz="skip";;
	*) wiz="$lang";;
	esac
	for i in $(find "$pkgdir/$wizdir" -name "resources_$wiz.properties"); do
		_move_from common "$i" "$wizdir"
	done

	logodir="usr/lib/libreoffice/share/Scripts/python/LibreLogo"
	case "$lang" in
	en_US) logo="en_[A-Z][A-Z]";;
	en_*) logo="skip";;
	*) logo="$lang";;
	esac
	for i in $(find "$pkgdir/$logodir" -name "LibreLogo_$logo.properties"); do
		_move_from common "$i" "$logodir"
	done
}

common() {
	pkgdesc="LibreOffice - common files"
	depends="$pkgname-lang-en_us $_fonts"

	_split common

	paxmark -m \
		"$subpkgdir"/usr/lib/libreoffice/program/soffice.bin \
		"$subpkgdir"/usr/lib/libreoffice/program/unopkg.bin

	mkdir -p "$subpkgdir/usr/share/appdata"
	mv "$pkgdir"/usr/share/appdata/*.xml "$subpkgdir/usr/share/appdata"

	# At this point there should only be empty directories left in
	# the "libreoffice" metapackage
	if [ -n "$(find "$pkgdir" -type f)" ]; then
		error "Files still in 'libreoffice' package:"
		find "$pkgdir" -type f | sed "s#^$pkgdir/#\\t#"
		return 1
	fi
}

gtk2() {
	pkgdesc="LibreOffice - GTK+2.0 GUI"
	depends="libreoffice-common"
	install_if="$pkgname-common=$pkgver-r$pkgrel gtk+2.0"
	_split gnome
}

base() {
	pkgdesc="LibreOffice - database frontend"
	depends="libreoffice-common"
	_split base
}

calc() {
	pkgdesc="LibreOffice - spreadsheet editor"
	depends="libreoffice-common"
	_split calc
}

draw() {
	pkgdesc="LibreOffice - drawing application"
	depends="libreoffice-common"
	_split draw
}

impress() {
	pkgdesc="LibreOffice - presentation application"
	depends="libreoffice-common"
	_split impress
}

math() {
	pkgdesc="LibreOffice - equation editor"
	depends="libreoffice-common"
	_split math
}

postgres() {
	pkgdesc="LibreOffice - connector for PostgreSQL database"
	depends="libreoffice-base"
	_split postgresql
}

writer() {
	pkgdesc="LibreOffice - word processor"
	depends="libreoffice-common"
	_split writer
}

sha512sums="8cf7bd4d8f81ee09f8a21c4ccb12f788c67cf1cba71e08b9b720f9e8b4ec2dfcc25452ef05b6b60529463241c2b23a70eb6a79a8f15b95e890a2ea9cbb458517  libreoffice-6.0.6.2.tar.xz
7b84e2ec964b02e5d2fe17f7879080571fce02d090cde0e09de07af5419c8b7879e88a53fd1231d1195e3d29a2919c33a7d85f11c469cd87670ad67921cf8b9b  libreoffice-dictionaries-6.0.6.2.tar.xz
fcc2ffeefde50abb80e539f7395efec6e022462b8b8938c2077df6fb3be217808cc2b96eb51539d5bf04fe0680807cb9af5f46f838bac6e6a5c88f90f88c5f2c  libreoffice-translations-6.0.6.2.tar.xz
89caab57774f85f3e58523c0debb57247a0f10959634bf296aaacc4928bcce6ea8ed9f167b267e2e0eeb52b69714eeb094fedfa3e810cf409dfbc7c5e49b7c13  libreoffice-help-6.0.6.2.tar.xz
10a1ee056ebce41b2f6d3863b220f8529f70b7fd159ff2d36967ad96d9653166737db4ba865c10769c37afad553f59fb9629437c0d8a1afbbff963fc36dbbf1a  linux-musl.patch
4aa595588226ac2060ae78f4f857db1b148735429a47389f75313df61cabf0819ad2add15c09cae5c5ebbce64852589ca89932c72281ba662ab53dddec4e1336  fix-execinfo.patch
ae36500897db9d758e95da1791ef2577d59b0e0865baba316b4a1f1f48ce9fd5e52fc3f14fa45aeeab73c5b93f57e89f9a42c996bdb3c85eb3beb2634543d85e  fix-includes.patch
c50272ec2cf0471e629bf817d23e5c51ff59e94961fab1ee107f43352409353a37f2988c1ff75f423c4c5aedb1e38b432849619700e25124a33c99a589d7a80b  gettext-tiny.patch
4ce3bacada3ab99dc09c6d29529bfc5e70eb287bcb61bc6e9a4ba4334cf77624121191d240f2a8f31b63e7510886904ce7842425752252e174478e0034988055  disable-crc-test.patch
887680966f8e8754d551ca7e4acc3bcae57c4fb835c49240fde05f65eb4282b8ad03deda3944b50e73b0da97ca8b28e7b1f1861907e6dbd012f5646e6d55efb4  disable-outdated-font-test.patch"
