# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=breeze-icons
pkgver=5.54.0
pkgrel=0
pkgdesc="Modern, coherent icon set for desktops"
url="https://www.kde.org/"
arch="noarch"
options="!check"  # 8,753 failures because it can't tell symlink from file.
license="LGPL-3.0+"
depends=""
makedepends="$depends_dev cmake extra-cmake-modules qt5-qtbase-dev"
checkdepends="fdupes"
subpackages="breeze-icons-dark:dark"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/breeze-icons-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

dark() {
	pkgdesc="Modern, coherent icon set for desktops - Darker and edgier"
	mkdir -p "$subpkgdir"/usr/share/icons
	mv "$pkgdir"/usr/share/icons/breeze-dark "$subpkgdir"/usr/share/icons/
}

sha512sums="60a2f07bcf3e8ce59b3860d4e74411e3b52af16c08ff3213ecdbebca8d1e9a2918ec5b24f67af0dcee0673108a3f2c8f8b6c04d2a829bdaeac300383fdd6dad5  breeze-icons-5.54.0.tar.xz"
