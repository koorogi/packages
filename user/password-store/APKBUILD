# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Johannes Matheis <jomat+alpinebuild@jmt.gr>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=password-store
pkgver=1.7.3
pkgrel=0
pkgdesc="Stores, retrieves, generates, and synchronizes passwords securely"
url="https://www.passwordstore.org"
arch="noarch"
options="!check"  # this is a shell script and has no tests
license="GPL-2.0+"
depends="bash gnupg libqrencode tree xclip"
depends_dev=""
makedepends="$depends_dev"
subpackages="$pkgname-doc
	$pkgname-contrib
	$pkgname-bash-completion:bashcomp
	$pkgname-fish-completion:fishcomp
	$pkgname-zsh-completion:zshcomp"
source="https://git.zx2c4.com/password-store/snapshot/$pkgname-$pkgver.tar.xz"

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" \
		WITH_ALLCOMP=yes install
}

contrib() {
	pkgdesc="Contrib files for pass"
	mkdir -p "$subpkgdir"/usr/share
	cp -a "$builddir"/contrib "$subpkgdir"/usr/share/$pkgname
}

bashcomp() {
	depends=""
	pkgdesc="Bash completions for $pkgname"
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"

	mkdir -p "$subpkgdir"/usr/share/bash-completion/completions
	mv "$pkgdir"/usr/share/bash-completion/completions/* \
		"$subpkgdir"/usr/share/bash-completion/completions/
}

fishcomp() {
	depends=""
	pkgdesc="Fish completions for $pkgname"
	install_if="$pkgname=$pkgver-r$pkgrel fish"

	mkdir -p "$subpkgdir"/usr/share/fish/completions/
	mv "$pkgdir"/usr/share/fish/vendor_completions.d/* \
		"$subpkgdir"/usr/share/fish/completions/
}

zshcomp() {
	depends=""
	pkgdesc="Zsh completions for $pkgname"
	install_if="$pkgname=$pkgver-r$pkgrel zsh"

	mkdir -p "$subpkgdir/usr/share/zsh/site-functions"
	mv "$pkgdir"/usr/share/zsh/site-functions/* \
		"$subpkgdir"/usr/share/zsh/site-functions
}

sha512sums="89755a7b02f05d75055a9fc14fd3f456c0a49ec31bdffd097a027f91228c64a98b18c1e80017aeda811773ae3287ff1b9737532da8ded06799d8fe3979ca06f7  password-store-1.7.3.tar.xz"
