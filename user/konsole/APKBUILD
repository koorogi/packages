# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=konsole
pkgver=18.12.2
pkgrel=0
pkgdesc="Terminal emulator for Qt/KDE"
url="https://konsole.kde.org/"
arch="all"
options="!check"  # Requires running DBus session bus.
license="GPL-2.0-only AND LGPL-2.1+ AND Unicode-DFS-2016"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kbookmarks-dev
	kcompletion-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev
	kcrash-dev kguiaddons-dev kdbusaddons-dev ki18n-dev kiconthemes-dev
	kinit-dev kio-dev knotifications-dev knotifyconfig-dev kparts-dev
	kpty-dev kservice-dev ktextwidgets-dev kwidgetsaddons-dev python3
	kwindowsystem-dev kxmlgui-dev kdbusaddons-dev knewstuff-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/konsole-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="3bf32dc9d39dbffb855a043728cca99b21819015a7ca1df56f17f18ec6b5cd5d53bd85f46381bafae72d3747d5b9e6beeff8a79e029800c5af599b169074bf68  konsole-18.12.2.tar.xz"
