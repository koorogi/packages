# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libkdcraw
pkgver=18.12.1
pkgrel=0
pkgdesc="RAW image file format support for KDE"
url="https://www.KDE.org/"
arch="all"
license="GPL-2.0+ AND LGPL-2.0+"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules libraw-dev"
subpackages="$pkgname-dev"
source="https://download.kde.org/stable/applications/$pkgver/src/libkdcraw-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="6adc597e93e6a299c50554c94fbf3bfcc934a97205af04ded7a5c84a980372e72eba23aba063548e5c6cb53a218bc04cbaefbd137450fade09939df578804ffb  libkdcraw-18.12.1.tar.xz"
