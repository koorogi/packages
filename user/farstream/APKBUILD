# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=farstream
pkgver=0.2.8
pkgrel=0
pkgdesc="Audio/Video communications framework"
url="https://www.freedesktop.org/wiki/Software/Farstream/"
arch="all"
options="!check"  # Tests are really really brittle and depend on networking.
license="LGPL-2.1+"
depends="gst-plugins-base gst-plugins-good"
depends_dev="glib-dev"
makedepends="$depends_dev gobject-introspection-dev gstreamer-dev
	gst-plugins-base-dev libnice-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://freedesktop.org/software/farstream/releases/farstream/farstream-$pkgver.tar.gz"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--with-package-origin="${DISTRO_NAME-:Adélie Linux} (${DISTRO_URL-:https://www.adelielinux.org/})"
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="7ec5c57f8778f4107cb628dbf411e38b726152cf78920127dff4423239ff7e4980b6b4f938abba2aa21ab984b1e3053e7e648611322a0ce94df0af576df99a7e  farstream-0.2.8.tar.gz"
