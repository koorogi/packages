# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=quaternion
pkgver=0.0.9.3
pkgrel=0
pkgdesc="Qt5-based Matrix chat client"
url="https://matrix.org/docs/projects/client/quaternion.html"
arch="all"
license="GPL-3.0+"
depends="qt5-qtquickcontrols"
makedepends="cmake libqmatrixclient-dev qt5-qtbase-dev qt5-qtdeclarative-dev
	qt5-qttools-dev"
subpackages=""
source="quaternion-$pkgver.tar.gz::https://github.com/QMatrixClient/Quaternion/archive/v$pkgver.tar.gz"
builddir="$srcdir/Quaternion-$pkgver"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="3836338cddc674b3a8b6e7d8376f0136db5c548788a0e56f27023fe7b238e2c07d77bf5e5b50e49cdb011c61f05700818b6a22a394ee8d52015f5f32b3fe4d47  quaternion-0.0.9.3.tar.gz"
