# Contriburo: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Michael Mason <ms13sp@gmail.com>
# Maintainer: 
pkgname=gnutls
pkgver=3.6.6
pkgrel=0
pkgdesc="A TLS protocol implementation"
url="http://www.gnutls.org/"
arch="all"
options="!check"  # https://gitlab.com/gnutls/gnutls/issues/560
license="LGPL-2.1+"
makedepends="libtasn1-dev libunistring-dev nettle-dev p11-kit-dev texinfo
	zlib-dev"
checkdepends="diffutils"
subpackages="$pkgname-dbg $pkgname-doc $pkgname-dev $pkgname-utils
	$pkgname-c++:xx"
_v=${pkgver%.*}
case $pkgver in
*.*.*.*) _v=${_v%.*};;
esac
source="https://www.gnupg.org/ftp/gcrypt/gnutls/v${_v}/$pkgname-$pkgver.tar.xz
	gnulib-tests-dont-require-gpg-passphrase.patch"

# secfixes:
#   3.5.13-r0:
#     - CVE-2017-7507

build() {
	cd "$builddir"
	LIBS="-lgmp" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-openssl-compatibility \
		--disable-rpath \
		--disable-static \
		--disable-guile \
		--disable-valgrind-tests \
		--without-included-libtasn1 \
		--enable-cxx \
		--enable-manpages \
		--enable-tests \
		--disable-full-test-suite \
		--disable-sslv2-support \
		--with-zlib \
		--with-p11-kit
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make -j1 DESTDIR="$pkgdir" install
}

utils() {
	pkgdesc="Command line tools for TLS protocol"
	mkdir -p "$subpkgdir"/usr/
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

xx() {
	pkgdesc="The C++ interface to GnuTLS"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/lib*xx.so.* "$subpkgdir"/usr/lib/
}

sha512sums="4ff34f38d7dc543bc5750d8fdfe9be84af60c66e8d41da45f6cffc11d6c6c726784fd2d471b3416604ca1f3f9efb22ff7a290d5c92c96deda38df6ae3e794cc1  gnutls-3.6.6.tar.xz
abc24ee59cc67805fe953535b0bae33080fc8b0bf788304377f6d10ec8c162c4cf203a69c98a4ba3483b4c60ed7a204433cc7db9b8190eddb0d68f6fb6dad52d  gnulib-tests-dont-require-gpg-passphrase.patch"
