# Dumb failures
# Expects permissions on /etc to be 40755
ext/standard/tests/file/006_error.phpt
# session_start() missing - needs session.so
# Test manually using the following:
# test.ini contains "extension=./modules/session.so"
# TEST_PHP_EXECUTABLE=sapi/cli/php sapi/cli/php run-tests.php -c test.ini $f
Zend/tests/unset_cv05.phpt
Zend/tests/unset_cv06.phpt
# Tests undefined behavior (integer underflow or overflow)
Zend/tests/dval_to_lval_32.phpt
Zend/tests/int_underflow_32bit.phpt
ext/date/tests/bug53437_var3.phpt
ext/date/tests/bug53437_var5.phpt
ext/date/tests/bug53437_var6.phpt

# General glibc/musl incompatibility related failures
# stdout printed in wrong order
ext/standard/tests/general_functions/ini_get_all.phpt
sapi/cgi/tests/005.phpt
# "Filename" instead of "File name" printed for ENAMETOOLONG
ext/standard/tests/strings/007.phpt
# glibc will throw EINVAL for popen with mode "rw" specifically,
# whereas musl only checks if the first character is 'r' or 'w'
ext/standard/tests/file/popen_pclose_error.phpt
# "Address in use" instead of "Address already in use" printed for EADDRINUSE
sapi/fpm/tests/socket-ipv4-fallback.phpt

# locale related failures
# LC_NUMERIC unsupported
ext/standard/tests/strings/sprintf_f_3.phpt
tests/lang/034.phpt
tests/lang/bug30638.phpt
# LC_ALL unsupported
ext/pcre/tests/locales.phpt
ext/standard/tests/array/locale_sort.phpt
ext/standard/tests/strings/setlocale_variation3.phpt
ext/standard/tests/strings/setlocale_variation4.phpt
ext/standard/tests/strings/setlocale_variation5.phpt
# LC_CTYPE unsupported
ext/standard/tests/strings/htmlentities02.phpt
ext/standard/tests/strings/htmlentities03.phpt
ext/standard/tests/strings/htmlentities04.phpt
ext/standard/tests/strings/htmlentities15.phpt
ext/standard/tests/strings/strtoupper.phpt
# LC_MONETARY unsupported
ext/standard/tests/strings/moneyformat.phpt
# locale: command not found
ext/standard/tests/strings/setlocale_basic1.phpt
ext/standard/tests/strings/setlocale_basic2.phpt
ext/standard/tests/strings/setlocale_basic3.phpt
ext/standard/tests/strings/setlocale_variation1.phpt
ext/standard/tests/strings/setlocale_variation2.phpt
# setlocale allows "en_US.invalid"
ext/standard/tests/strings/setlocale_error.phpt

# strftime and strptime related failures
# strftime %Z (timezone abbreviation) returns a single space
# This appears to be a bug in php and not musl
ext/date/tests/bug27780.phpt
ext/date/tests/bug32555.phpt
ext/date/tests/bug33532.phpt
# strptime returns tm with tm_wday and tm_yday == 0
# This appears to be a bug with *musl* and not php
# and also strftime returns a space for %Z as before
ext/standard/tests/time/strptime_basic.phpt
# strptime returning NULL when %Z is used (glibc extension)
ext/standard/tests/time/strptime_parts.phpt
# strftime("%q") returns false instead of some string
# This is because glibc will return "%q" and musl will return ""
ext/date/tests/009.phpt

# crypt() related failures
# crypt() returns "*" instead of "*0" or "*1"
ext/standard/tests/strings/bug51059.phpt
ext/standard/tests/crypt/bcrypt_invalid_algorithm.phpt
ext/standard/tests/crypt/bcrypt_invalid_cost.phpt
ext/standard/tests/strings/crypt_blowfish_variation1.phpt
ext/standard/tests/strings/crypt_blowfish_variation2.phpt
# crypt() returning incorrect results in general, in addition to the above
# and unexpected deprecation warning for invalid DES salt
ext/standard/tests/strings/crypt_blowfish.phpt
# crypt() has unexpected deprecation warning for invalid DES salt
ext/standard/tests/strings/crypt_des_error.phpt
# crypt() *missing* deprecation warnings for invalid DES salt
ext/standard/tests/crypt/des_fallback_invalid_salt.phpt

# These two are marked as XFAIL and do as such normally
# But with --enable-debug, they pass...
#sapi/fpm/tests/010.phpt
#sapi/fpm/tests/015.phpt

# Times out on builders but runs fine manually
ext/zlib/tests/inflate_add_basic.phpt
sapi/cli/tests/upload_2G.phpt
