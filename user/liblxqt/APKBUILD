# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=liblxqt
pkgver=0.14.0
pkgrel=0
pkgdesc="Core LXQt library"
url="https://lxqt.org"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtx11extras-dev
	qt5-qttools-dev libxscrnsaver-dev libqtxdg-dev kwindowsystem-dev
	polkit-qt-1-dev lxqt-build-tools>=0.6.0"
subpackages="$pkgname-dev"
source="https://github.com/lxqt/liblxqt/releases/download/$pkgver/liblxqt-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	mkdir -p build && cd build
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} ..
	make
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install
}

sha512sums="ed4009ae7a0f556c84131083946a62a8a91d66c12a75902e2841b7a8388cd93cd493e42674bbf34fe5d20f8d86685ad2ad197c0b2222d7a08558bc8a2946ca86  liblxqt-0.14.0.tar.xz"
