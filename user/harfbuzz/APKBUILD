# Contributor: Sören Tempel <soeren+alpinelinux@soeren-tempel.net>
# Maintainer: 
pkgname=harfbuzz
pkgver=1.8.8
pkgrel=1
pkgdesc="Text shaping library"
url="https://www.freedesktop.org/wiki/Software/HarfBuzz"
arch="all"
options="!check"  # tests depend on certain freetype behaviours
license="MIT"
depends=""
makedepends="cairo-dev freetype-dev glib-dev gobject-introspection-dev icu-dev
	graphite2-dev"
checkdepends="python3"
subpackages="$pkgname-dev $pkgname-icu"
source="http://www.freedesktop.org/software/$pkgname/release/$pkgname-$pkgver.tar.bz2"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--disable-static \
		--with-glib \
		--with-gobject \
		--with-graphite2 \
		--with-icu \
		--with-truetype
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

dev() {
	default_dev
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

icu() {
	pkgdesc="Harfbuzz ICU support library"
	replaces="harfbuzz"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/lib*icu.so.* "$subpkgdir"/usr/lib/
}

sha512sums="eb96cd710571a96473b20bc9a01dadf2a3c11224497e52c63368e8edec64a8eb7085dd847c78111b798a1e8a6a950f0a04c930209822aabf13cf86d7a53b1f79  harfbuzz-1.8.8.tar.bz2"
