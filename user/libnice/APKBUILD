# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libnice
pkgver=0.1.15
pkgrel=0
pkgdesc="GLib-based Interactive Connectivity Establishment (ICE) library"
url="https://nice.freedesktop.org/wiki/"
arch="all"
license="MPL-1.1 AND LGPL-2.1-only"
depends=""
depends_dev="glib-dev"
makedepends="$depends_dev gobject-introspection-dev gstreamer-dev openssl-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://nice.freedesktop.org/releases/libnice-$pkgver.tar.gz
	dont-error-on-socket-close.patch
	"

build() {
	cd "$builddir"
	LDFLAGS="-Wl,-z,stack-size=1048576" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-static
	make
}

check() {
	cd "$builddir"
	# multi-make causes test-drop-invalid test to fail
	make -j1 check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="60a8bcca06c0ab300dfabbf13e45aeac2085d553c420c5cc4d2fdeb46b449b2b9c9aee8015b0662c16bd1cecf5a49824b7e24951a8a0b66a87074cb00a619c0c  libnice-0.1.15.tar.gz
c81aff0f8a674315997f2ecc1f0cbc501b54d49c142949aee68af42aaccf2a2f61d5eb46ce8c123b05fb98c2cd5ef5751b9228783e5e221b12be06b805da0ad3  dont-error-on-socket-close.patch"
