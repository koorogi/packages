# Contributor: Michael Mason <ms13sp@gmail.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=iperf3
_pkgname=iperf
pkgver=3.6
pkgrel=0
pkgdesc="A tool to measure IP bandwidth using UDP or TCP"
url="https://software.es.net/iperf/"
arch="all"
license="BSD-3-Clause"
depends=""
makedepends=""
install=""
subpackages="$pkgname-doc $pkgname-openrc"
source="$pkgname-$pkgver.tar.gz::https://downloads.es.net/pub/$_pkgname/$_pkgname-$pkgver.tar.gz
	$_pkgname.initd
	$_pkgname.confd

	build-fixes.patch
	remove-pg-flags.patch
	"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	cd "$builddir"
	CFLAGS="$CFLAGS -D_GNU_SOURCE"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install

	install -Dm755 "$srcdir"/$_pkgname.initd \
		"$pkgdir"/etc/init.d/$_pkgname
	install -Dm644 "$srcdir"/$_pkgname.confd \
		"$pkgdir"/etc/conf.d/$_pkgname
}

sha512sums="322a6893ad28c6ce9c9849e8a0326408ed76f09bbb13591c01c3d051bd4777a2acdb8f5032b9d1bb37d47ad3818d8b18ab6c0be4d40559040eb7e57bbfc06f39  iperf3-3.6.tar.gz
339fb04b41cce11e8ea8694d95c13af9c88e0d1143356d913ec810c9b11efa27212d585b5dcc49cc2eb860a6d8bc557092c8659d7d5cfe532c5afdb36f8eedf8  iperf.initd
fb5e155fff568a72488cc1511d30358835a48bcce309f8f46d040160932b4a4ccb85040c27c60dee477900a25bd34c50eac21d1d0bea984b40faa401be2ba7e8  iperf.confd
126043503eebf84bccf26758f18715eaf81131e2fc9ef1fe829e55acb2f14e97db18e9b890b8bf13c230ac80c7537615272af4e072808e25bde8ddd6f326f198  build-fixes.patch
ed0fb3a3af0dae3e9f58ff6643cb15defdd31f65cf966a6013fdf2a7fd67a7f8ef522a7c8f3269bd43d5ca991a86d758526c384ebc0984b1403bb24f61a1779b  remove-pg-flags.patch"
