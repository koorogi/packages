# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=nextcloud-client
pkgver=2.5.0_beta2
_ver="${pkgver%_beta2}-beta2"
pkgrel=0
pkgdesc="Nextcloud desktop client"
url="https://github.com/nextcloud/desktop"
arch="all"
options="!checkroot"
license="GPL-2.0+ AND LGPL-2.1+ AND Public-Domain AND MIT AND (Custom:Digia-Qt OR LGPL-2.1-only WITH Qt-LGPL-exception-1.1) AND (Custom:Digia-Qt OR LGPL-2.1-only WITH Qt-LGPL-exception-1.1 OR GPL-3.0-only)"
depends=""
makedepends="cmake qt5-qttools-dev qtkeychain-dev zlib-dev
	openssl-dev sqlite-dev qt5-qtsvg-dev"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/nextcloud/desktop/archive/v$_ver.tar.gz
	no-webengine.patch
	openssl.patch"
builddir="$srcdir/desktop-$_ver"

build() {
	cd "$builddir"
	cmake \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_SYSCONFDIR="/etc/$pkgname" \
		-DNO_SHIBBOLETH=1 \
		-DWITH_CRASHREPORTER=bool:OFF \
		-DUNIT_TESTING=bool:ON
	make
}

check() {
	cd "$builddir"
	make test
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="ed5ec8c0fd79d3f7f843ef1aefb9b94088b85dacca442388bc9a42e1ddbf1ee90482595135ffaffc85e40d223406964c903949ca1c2161fa0f6a2d6770a77cea  nextcloud-client-2.5.0_beta2.tar.gz
6bd83fdee02eabe7ae29fb1a677f62d4a416ec553a0f8c66b7544cafb9201dd7d8b04dc6fb21f447f6c1ece13b06f5d3cba57ac71b211e166607ef15350b3e57  no-webengine.patch
e323a1074f8ac96667a420f076fdfc988e2fd97cdacd05d83ac54b467b567f5adbf635e7c4fb0414af0012b4016cc4c13441cb35ed3976bc970e514e81b65fd4  openssl.patch"
