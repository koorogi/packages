# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Andrew Manison <amanison@anselsystems.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=mutt
pkgver=1.10.1
pkgrel=0
pkgdesc="Small but very powerful text-mode email client"
url="http://www.mutt.org"
arch="all"
options="suid !check"  # No test suite.
license="GPL-2.0+ AND LGPL-2.0+ AND LGPL-2.1+ AND Public-Domain"
makedepends="cyrus-sasl-dev gdbm-dev gettext-tiny gpgme-dev
	libidn-dev ncurses-dev openssl-dev perl"
subpackages="$pkgname-doc $pkgname-lang"
source="https://bitbucket.org/$pkgname/$pkgname/downloads/$pkgname-$pkgver.tar.gz"

build() {
	cd "$builddir"
	ISPELL=/usr/bin/hunspell \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--enable-imap \
		--enable-pop \
		--enable-smtp \
		--enable-hcache \
		--enable-gpgme \
		--enable-sidebar \
		--enable-smime \
		--with-curses \
		--with-mailpath=/var/spool/mail \
		--with-docdir=/usr/share/doc/$pkgname \
		--without-included-gettext \
		--with-ssl \
		--with-sasl
	make
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install

	rm "$pkgdir"/etc/*.dist \
		"$pkgdir"/etc/mime.types \
		"$pkgdir"/usr/bin/muttbug \
		"$pkgdir"/usr/bin/flea

	# Don't tamper with the global configuration file.
	# Many options set in the global config cannot be
	# overwritten in the users configuration file.
	# Example: Resetting colors isn't possible.
	install -Dm644 contrib/gpg.rc \
		"$pkgdir"/etc/Muttrc.gpg.dist
}

sha512sums="13509ff159c65e23614696967b4bd8baf7b8e16c0e85cfd89e2df8201de2dc97e6c4a426f5919f05051dee2b14f847c7203e0b236931d3cdf81dbbb221551726  mutt-1.10.1.tar.gz"
