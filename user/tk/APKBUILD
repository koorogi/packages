# Maintainer: 
pkgname=tk
pkgver=8.6.9.1
pkgrel=0
pkgdesc="GUI toolkit for the Tcl scripting language"
url="http://tcl.sourceforge.net/"
arch="all"
options="!check"  # Requires a running X11 server.
license="TCL"
depends=
depends_dev="tcl-dev libx11-dev libxft-dev fontconfig-dev"
makedepends="$depends_dev libpng-dev"
subpackages="$pkgname-doc $pkgname-dev"
source="https://downloads.sourceforge.net/sourceforge/tcl/$pkgname$pkgver-src.tar.gz
	"
_major=8.6
builddir="$srcdir"/tk8.6.9/unix

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--mandir=/usr/share/man
	make
}

package() {
	cd "$builddir"
	export LD_LIBRARY_PATH="$builddir"
	make -j1 INSTALL_ROOT="${pkgdir}" install install-private-headers

	ln -sf wish${_major} "${pkgdir}"/usr/bin/wish
	install -Dm644 ../license.terms \
		${pkgdir}/usr/share/licenses/${pkgname}/LICENSE

	# remove buildroot traces
	find "$pkgdir" -name '*Config.sh' | xargs sed -i -e "s#${srcdir}#/usr/src#"

	# move demos to -doc directory
	mkdir -p "$pkgdir"/usr/share/doc/$pkgname/examples/
	mv "$pkgdir"/usr/lib/tk${_major}/demos/ \
		"$pkgdir"/usr/share/doc/$pkgname/examples/
}

dev() {
	default_dev
	cd $pkgdir
	for i in $(find . -name '*.c' -o -name '*Config.sh'); do
		mkdir -p "$subpkgdir"/${i%/*}
		mv $i "$subpkgdir"/${i%/*}/
	done
}

sha512sums="b9c811ffc8326331ae03c6fb25ea71f7a5eaeebd9d5a16a51a1671d0f0422268bd351b077e17ae925f0a7eddac9642aa640658615c52d4269c299373af031a92  tk8.6.9.1-src.tar.gz"
