# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=command-not-found
pkgver=0.3
pkgrel=0
pkgdesc="Provide suggestions for missing commands via APK"
url="https://git.dereferenced.org/kaniini/command-not-found"
arch="noarch"
options="!check"  # No test suite.
license="MIT"
depends="apk-tools"
makedepends=""
subpackages="$pkgname-bash $pkgname-zsh"
source="$pkgname-$pkgver.tar.gz::https://git.dereferenced.org/kaniini/command-not-found/archive/v$pkgver.tar.gz
	explicit-apk-path.patch
	"
builddir="$srcdir/$pkgname"

package() {
	cd "$builddir"
	install -d -D -m755 "$pkgdir"/usr/libexec
	install -m755 command-not-found.sh "$pkgdir"/usr/libexec/command-not-found
}

bash() {
	pkgdesc="$pkgdesc (Bash shell support)"
	install_if="$pkgname=$pkgver-r$pkgrel bash"
	cd "$builddir"
	install -d -D -m755 "$subpkgdir"/etc/profile.d/ 
	install -m755 profiles/command-not-found.ash "$subpkgdir"/etc/profile.d/command-not-found.sh
}

zsh() {
	pkgdesc="$pkgdesc (Z shell support)"
	install_if="$pkgname=$pkgver-r$pkgrel zsh"
	cd "$builddir"
	install -d -D -m755 "$subpkgdir"/etc/zprofile.d/ 
	install -m755 profiles/command-not-found.zsh "$subpkgdir"/etc/zprofile.d/command-not-found.zsh
}

sha512sums="a9fc6cf063195abf4bbf015f770b93fb23e2afc0efbed10a54adc1e0d38d7f9a57ab5817a52e483dd01aa21888e76f5aaefdbf89c835f227be2f150848ddb7ad  command-not-found-0.3.tar.gz
37e9106924c595a514b5df43910f757b0e2867aa24c5bc2f4eb675eb110ce8c73d95c69c059a8a895a3ca58f441718261677c632acda5295c040a892cc024190  explicit-apk-path.patch"
