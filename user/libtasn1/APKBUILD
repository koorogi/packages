# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libtasn1
pkgver=4.13
pkgrel=0
pkgdesc="Highly portable ASN.1 library"
url="https://www.gnu.org/software/libtasn1/"
arch="all"
license="LGPL-2.1+"
makedepends="texinfo"
subpackages="$pkgname-dev $pkgname-doc $pkgname-tools"
source="ftp://ftp.gnu.org/gnu/$pkgname/$pkgname-$pkgver.tar.gz
	"

# secfixes:
#   4.13-r0:
#   - CVE-2018-6003
#   4.12-r1:
#   - CVE-2017-10790

build() {
	cd "$builddir"
	CFLAGS="-Wno-error=inline" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make -j1
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

tools() {
	pkgdesc="Tools for parsing and manipulating ASN.1"
	license="GPL-3.0+"
	mkdir -p "$subpkgdir"/usr
	mv -i "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

sha512sums="bf5b60a296795e0a8a4a658c0106492393aa7ce698e785256b3427c17215c2a5b6178a61a2043c93ea4334f754eabece20221ac8fef0fd5644086a3891d98a9f  libtasn1-4.13.tar.gz"
