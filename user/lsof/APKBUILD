# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=lsof
pkgver=4.91
pkgrel=0
pkgdesc="LiSt Open Files"
url="https://people.freebsd.org/~abe"
arch="all"
license="zlib-acknowledgement"
subpackages="$pkgname-doc"
makedepends="linux-headers cmd:which utmps-dev"
source="ftp://lsof.itap.purdue.edu/pub/tools/unix/lsof/${pkgname}_${pkgver}.tar.gz
	utmps.patch
	test-enable-suite.patch
	test-linux-sysmacros.patch
	test-disable-nfs-test.patch"
builddir="$srcdir/${pkgname}_${pkgver}"

unpack() {
	default_unpack
	cd "$builddir"
	tar xf "${pkgname}_${pkgver}_src.tar"
}

prepare() {
	default_prepare
	cd "$builddir/${pkgname}_${pkgver}_src"

	sed -i "s|/\* #define\tHASSECURITY\t1 \*/|#define\tHASSECURITY\t1|" \
		dialects/linux/machine.h
	sed -n 6,30p lsof.h > COPYING
}

build () {
	cd "$builddir/${pkgname}_${pkgver}_src"

	./Configure -n linux
	make
}

check() {
	cd "$builddir/${pkgname}_${pkgver}_src/tests"

	# The LTsock test will fail if /etc/hosts is not set correctly.
	# Make sure that 127.0.0.1 and ::1 have aliases corresponding to
	# `hostname`.
	printf "y" | make
	make opt
}

package() {
	cd "$builddir/${pkgname}_${pkgver}_src"

	install -Dm755 lsof "$pkgdir/usr/bin/lsof"
	install -Dm644 lsof.8 "$pkgdir/usr/share/man/man8/lsof.8"
	install -Dm644 COPYING "$pkgdir/usr/share/licenses/lsof/COPYING"
}

sha512sums="95937afda1ba58e6636b2a2661d819480ec42b4beafc0e69e6132d7fdffd4e3974a95036a02c6fd8baf92089c73ac969cdc00a297bf0249257867d695bff2f94  lsof_4.91.tar.gz
dfbfcfd41a4bccf3335ed01cc2dc9a11b6badd3149878d89ef5fed322a914d3d8341c68783140a4973d007907ce54b4438f011c26289df9e27d2d487a8f6b0b0  utmps.patch
34d9573f90d942c717e3f3ff17283a66981c4a2e8e886746b591f980142d66115bc7afe6bdd0fc660ea6c1b42f591b161b9535e5849b8757e79546c197ccfdda  test-linux-sysmacros.patch
7f3e9b2850af54a5391db6a8f7eaa22329217fd559d3d41ee3bcd33b07b3526d17da75a542d28b013111bc5d5712a718e961aa4fe84ca3c5e73a97e0518ba2ec  test-enable-suite.patch
d299e2070415168f75a9ee6424e18c1496f6d1fd5430b13a92159422c9df58050ac63e22cebd13a5228207f5960cf2cde5830a46c9ec7a4c216f5e6fb71887f4  test-disable-nfs-test.patch"
