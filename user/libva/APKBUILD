# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libva
pkgver=2.3.0
pkgrel=0
pkgdesc="Video Acceleration (VA) API for Linux"
url="https://github.com/intel/libva"
arch="all"
options="!check"  # No test suite.
license="MIT"
depends=""
depends_dev="mesa-dev"
makedepends="$depends_dev autoconf automake libtool"
subpackages="$pkgname-dev"
source="https://github.com/intel/${pkgname}/releases/download/${pkgver}/${pkgname}-${pkgver}.tar.bz2"

prepare() {
	cd "$builddir"
	default_prepare
	# we need to regen the configure script which will unconditionally
	# depend on wayland scanner otherwise
	libtoolize --force && aclocal -I m4 && autoconf \
		&& automake --add-missing
}

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--enable-x11 \
		--disable-wayland \
		--disable-static \
		--enable-shared
	make
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="e6e8b5e5c3dcfc4ad11560828c849ff522e3cd8db6763fe8d2fcec41c05461b3f354aaba82c7c78b8623f3d1c842851209fbbd046d027a0b88c6d9861e2ecfd3  libva-2.3.0.tar.bz2"
