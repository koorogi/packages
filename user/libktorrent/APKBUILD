# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libktorrent
pkgver=2.1
pkgrel=0
pkgdesc="Torrent handling library for KDE"
url="https://www.kde.org/"
arch="all"
options="!check"  # All tests require X11
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev karchive-dev kcrash-dev kio-dev solid-dev gmp-dev
	boost-dev qca-dev libgcrypt-dev"
makedepends="cmake extra-cmake-modules $depends_dev ki18n-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/ktorrent/5.1/libktorrent-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="28d7a4b8ee45ddcbadb3ca9d6e568120803081b0ecd3e9436ed721860717b98538c522653ea47975123f0819548d0cabe1973baffed887fcbbca11d84ea5ebe4  libktorrent-2.1.tar.xz"
