# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=lighttpd
pkgver=1.4.52
pkgrel=0
pkgdesc="A secure, fast, compliant and very flexible web-server"
url="http://www.lighttpd.net/"
arch="all"
license="BSD-3-Clause"
install="$pkgname.pre-install $pkgname.pre-upgrade"
pkgusers="lighttpd"
pkggroups="lighttpd"
makedepends="attr-dev bzip2-dev flex gamin-dev libev-dev libxml2-dev
	openldap-dev openssl-dev pcre-dev sqlite-dev zlib-dev"
subpackages="$pkgname-doc $pkgname-dbg $pkgname-mod_auth $pkgname-openrc
	$pkgname-mod_webdav"
source="http://download.lighttpd.net/lighttpd/releases-1.4.x/$pkgname-$pkgver.tar.xz
	$pkgname.initd
	$pkgname.confd
	$pkgname.logrotate
	lighttpd.conf
	mime-types.conf
	mod_cgi.conf
	mod_fastcgi.conf
	mod_fastcgi_fpm.conf
	"

build() {
	cd "$builddir"

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--disable-dependency-tracking \
		--enable-lfs \
		--libdir=/usr/lib/lighttpd \
		--without-mysql \
		--with-attr \
		--with-fam \
		--with-webdav-props \
		--with-webdav-locks \
		--without-gdbm \
		--with-bzip2 \
		--with-ldap \
		--with-openssl \
		--with-libev
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"

	make DESTDIR="$pkgdir" install

	# create dirs
	install -d -m755 -o lighttpd -g lighttpd \
		"$pkgdir"/var/log/lighttpd/ \
		"$pkgdir"/var/lib/lighttpd/cache/compress
	install -d -m755 \
		"$pkgdir"/etc/lighttpd/ \
		"$pkgdir"/var/www/localhost/htdocs

	# lighttpd
	install -D -m755 "$srcdir"/lighttpd.initd \
		"$pkgdir"/etc/init.d/lighttpd
	install -D -m644 "$srcdir"/lighttpd.confd \
		"$pkgdir"/etc/conf.d/lighttpd
	install -D -m644 "$srcdir"/lighttpd.logrotate \
		"$pkgdir"/etc/logrotate.d/lighttpd

	# config files
	local i; for i in lighttpd.conf mime-types.conf mod_cgi.conf \
		mod_fastcgi.conf mod_fastcgi_fpm.conf
	do
		install -m644 "$srcdir"/$i "$pkgdir"/etc/lighttpd/$i
	done
}

_mv_mod() {
	mkdir -p "$subpkgdir"/usr/lib/lighttpd
	while [ $# -gt 0 ]; do
		mv "$pkgdir"/usr/lib/lighttpd/$1.so \
			"$subpkgdir"/usr/lib/lighttpd/
		shift
	done
}

mod_auth() {
	pkgdesc="Authentication module for lighttpd"
	_mv_mod mod_auth
}

mod_webdav() {
	pkgdesc="WebDAV module for lighttpd"
	_mv_mod mod_webdav
}

sha512sums="3c604f441c001641681b958012524c9a2e801314b07d9741d4b5e086e7585d676516e3fe587e0ff69f1f937c11b9a290f2173866d6b90019117b6be299972a72  lighttpd-1.4.52.tar.xz
f2f3c5c7731550237fd75a8de66275f427eaf897cffff7ac7ef44178328ad8fad6c4ec6654759bfc665cbaf7991ddcdf0aaa916831c8b6aa440192d57b242038  lighttpd.initd
9d2ab5deb7353ebf290e90936b511941df440859c78589d0bcf130ef69a5e9c79e4d318548b6b118df002083c46f7476230a28954b7a10a9dbd05040e02b1291  lighttpd.confd
0536b4f21d2e8659f7831b45998c13d9f6051ae7ecde13be01f372f837d255bfc4e211de48a7686cc743d53aa9c08ab3f10ec19788896dcf8356b90053ca7a16  lighttpd.logrotate
e56ee836fa815c98c711f9381a8552ca94e1841aee5ddeee83631c385ccc556e966331499f4784982385f7ed4177062d3349705fd24de2ec5f1544ab1cc424de  lighttpd.conf
a3f2f5763885d7e4f510491b24164e34aaf62bb02daa12991575dc64335c12668355af5bb8d6ce191eb4e9cce95324b1f7c9ba61b323b4e7b50a1e03e021afcf  mime-types.conf
27cc638d8068dcf47bd9db44943d1db6c6f4e8e6abd6b42af7cea004b1c093440068541d98c68f8bea70b956713adaf8ed59a4b642dea826ee8620a05f8cfde5  mod_cgi.conf
1d15b84c03fb648a0e67ab5c5411b85478b4454c44bc2959cc96d1700eeadd7ff429520a5f1550db6527267646622dccd3d47d3fd1258869fccaf5c22d4ad4b2  mod_fastcgi.conf
f9efc4b70d825600f5356c30e57d0b6cac11c01739337f7192c09c2cfd96cb76c8328b11d818ea4c2addc1a6d253975b84700106ae75854d55d0df73e220bd2b  mod_fastcgi_fpm.conf"
