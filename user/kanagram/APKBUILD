# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kanagram
pkgver=18.12.2
pkgrel=0
pkgdesc="Letter order (anagram) game"
url="https://www.kde.org/applications/education/kanagram/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	ki18n-dev kcrash-dev sonnet-dev kconfig-dev kconfigwidgets-dev kio-dev
	kcoreaddons-dev kdeclarative-dev kdoctools-dev knewstuff-dev
	libkeduvocdocument-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kanagram-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="7e8f64b4ae9b77346f94fef8b8c00b2d6dade8e7bdf582e9b4fdb6881daf4eef45c4644a29e4406b6c3cf4395be88a6c234688583dd8b37c3f1c11c29860e8b8  kanagram-18.12.2.tar.xz"
