# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=lxqt-sudo
pkgver=0.14.0
pkgrel=0
pkgdesc="Graphical LXQt utility for sudo/su"
url="https://lxqt.org"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends="sudo"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev lxqt-build-tools>=0.6.0
	liblxqt-dev>=${pkgver%.*}.0 qt5-qttools-dev kwindowsystem-dev"
subpackages="$pkgname-doc"
source="https://github.com/lxqt/lxqt-sudo/releases/download/$pkgver/lxqt-sudo-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	mkdir -p build && cd build
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} ..
	make
}

check() {
	cd "$builddir"/build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install
}

sha512sums="06e8749dc11f48d6d76805fc2d93b04194287cb4d95c4640c80ca186f948d6b6585df5bab7d8b0fb0f23f4b07ffd057048afddddb820a3961d37bc1ce9cd3694  lxqt-sudo-0.14.0.tar.xz"
