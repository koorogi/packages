# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=labplot
pkgver=2.5.0
pkgrel=2
pkgdesc="Interactive tool for graphing and analysis of scientific data"
url="https://www.kde.org/applications/education/labplot/"
arch="all"
license="GPL-2.0-only"
depends="shared-mime-info"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev kconfig-dev
	karchive-dev kcompletion-dev kconfigwidgets-dev kcoreaddons-dev kio-dev
	kdoctools-dev ki18n-dev kiconthemes-dev kdelibs4support-dev kxmlgui-dev
	knewstuff-dev ktextwidgets-dev kwidgetsaddons-dev gsl-dev fftw-dev
	qt5-qtserialport-dev syntax-highlighting-dev bison libexecinfo-dev
	cantor-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/labplot/$pkgver/labplot-$pkgver.tar.xz
	liborigin-endian.patch
	"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=Debug \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS -D_GNU_SOURCE" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	# gives incorrect results
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E fittest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="a4b285917e30b0ac00cb8c8ad6827ba3884d95fccc0511a0a317bb2d637e48e6579929c45cfb8bba737bb5a01472a2baa9d812a688730ac4fa40b23cc6625eba  labplot-2.5.0.tar.xz
d251300ca0992637453ef12021f3fa4ba0ed5651a19b27bee7573f50b101aaa787544c035bf0d00b9a9ef14156536ce027879d605ede228761754b762db88ded  liborigin-endian.patch"
