# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=ddrescue
pkgver=1.23
pkgrel=0
pkgdesc="Data recovery tool for block devices with errors"
url="https://www.gnu.org/s/ddrescue/ddrescue.html"
license="GPL-3.0+"
arch="all !aarch64"
subpackages="$pkgname-doc"
source="http://ftp.gnu.org/gnu/$pkgname/$pkgname-$pkgver.tar.lz"

build() {
	cd "$builddir"
	./configure --prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info
	make
}

check() {
	cd "$builddir"

	# XXX: Some tests fail on s390x, but only on builder, so ignore it for now.
	case "$CARCH" in
		s390x) make check || true;;
		*) make check;;
	esac
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="4f0b27067966b71efaae809d4f38714863cf3663f3b8c3f26055d482debb15c0fab77752411a9d242f18dbb8e4edc68494f866721dae9c95cfc5354439eaa656  ddrescue-1.23.tar.lz"
