# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=ksudoku
pkgver=18.12.2
pkgrel=0
pkgdesc="Desktop Sudoku (symbol placement / logic) game"
url="https://games.kde.org/game.php?game=ksudoku"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	qt5-qtsvg-dev karchive-dev kconfig-dev kconfigwidgets-dev kcrash-dev
	kcoreaddons-dev kdoctools-dev kguiaddons-dev ki18n-dev kiconthemes-dev
	kio-dev kjobwidgets-dev kwidgetsaddons-dev kxmlgui-dev libkdegames-dev
	glu-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/ksudoku-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="23031ef64f76a425fa5657ba088a6a6cebbfabd3d53e574a899ac9020072759f371e787b0eb425bb12560aaf1dd673499316410b4c1d67ea887c53b60a7cf5ef  ksudoku-18.12.2.tar.xz"
