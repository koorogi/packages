# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=accountsservice
pkgver=0.6.54
pkgrel=0
pkgdesc="D-Bus service for accessing user account information"
url="https://www.freedesktop.org/wiki/Software/AccountsService/"
arch="all"
license="GPL-3.0+ AND GPL-2.0+"
depends="dbus"
makedepends="cmake dbus-dev glib-dev gobject-introspection-dev meson ninja
	polkit-dev utmps-dev xmlto"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://www.freedesktop.org/software/accountsservice/accountsservice-$pkgver.tar.xz
	disable-msgfmt
	"

build() {
	cd "$builddir"
	CPPFLAGS="-D_PATH_WTMPX=\\\"/run/utmps/wtmp\\\"" meson \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		-Dadmin_group=wheel \
		-Ddocbook=true \
		-Dsystemd=false \
		-Dsystemdsystemunitdir=no \
		. output
	mkdir -p output/data
	# msgfmt(1) XML error
	cp data/org.freedesktop.accounts.policy.in \
		output/data/org.freedesktop.accounts.policy
	patch -Np1 < "$srcdir"/disable-msgfmt
	ninja -C output
}

check() {
	cd "$builddir"
	ninja -C output test
}

package() {
	cd "$builddir"
	DESTDIR="$pkgdir" ninja -C output install
}

sha512sums="5fbcccc286dba60efba905aa79b1b51a5478fe52bf2e256d40d9fc2d7a311aea4ce397365045659a4ef2ecb2cc079130fb1d0107749c3c906c394517056ac1c0  accountsservice-0.6.54.tar.xz
d3cd21c871f66359aae0b4688aeb5f31ba124579212350870540344a489299413a13ad4b66872a9fb549ed3b3664dfbadd03c9b20df3714cb4d26e3f2cf107ce  disable-msgfmt"
