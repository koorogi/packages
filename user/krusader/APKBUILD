# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=krusader
pkgver=2.7.1
pkgrel=0
pkgdesc="Advanced, twin-panel (commander style) file manager"
url="https://krusader.org/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev karchive-dev kcodecs-dev
	kbookmarks-dev kcompletion-dev kcoreaddons-dev kconfig-dev kdoctools-dev
	ki18n-dev kiconthemes-dev kitemviews-dev kio-dev knotifications-dev
	kparts-dev solid-dev ktextwidgets-dev kwallet-dev kwidgetsaddons-dev
	kwindowsystem-dev kxmlgui-dev kguiaddons-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/krusader/$pkgver/krusader-$pkgver.tar.xz
	posix-headers.patch"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="860908fd960ae45d298ade30bd4962eea44d126b5a3bb28d04540a0e4fbc0645de2394490fdb624b5fcc7005841c0ce392cda896dc4b1d3d2de3a1e44171222d  krusader-2.7.1.tar.xz
6df1a858c896d91d2f09a0cf49a90177655cf21bd7fe18c5b82e0805f81d3c121978b932a2f139d2e0e4ca8d9741d181274b968628e905f87557b6ad43f05a59  posix-headers.patch"
