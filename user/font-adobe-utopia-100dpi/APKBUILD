# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=font-adobe-utopia-100dpi
pkgver=1.0.4
pkgrel=0
pkgdesc="100dpi Utopia X11 font from Adobe"
url="https://www.X.Org/"
arch="noarch"
# Okay.
# This is really hairy, but Fedora Legal says the TUG license can apply to the
# X11 distribution[1][2]; it's almost MIT style, but you have to rename the
# font if you modify it in any way.
# [1]: https://fedoraproject.org/wiki/Legal_considerations_for_fonts
# [2]: https://src.fedoraproject.org/cgit/rpms/xorg-x11-fonts.git/tree/xorg-x11-fonts.spec
license="Utopia"
depends="encodings font-alias fontconfig mkfontdir"
makedepends="bdftopcf font-util-dev util-macros"
subpackages=""
source="https://www.x.org/releases/individual/font/font-adobe-utopia-100dpi-$pkgver.tar.bz2"

prepare() {
	cd "$builddir"
	default_prepare
	update_config_sub
}

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
	find "$pkgdir" -name fonts.dir -exec rm {} +
}

sha512sums="fecb9a9bde99a82787d12779ea579c8696dcce168acd74b478a879ae24d421d5978d5f69da651e8ff3b25dca355960aaa19f69f2bf61e540464887e546a7b94b  font-adobe-utopia-100dpi-1.0.4.tar.bz2"
