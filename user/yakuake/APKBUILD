# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=yakuake
pkgver=3.0.5
pkgrel=0
pkgdesc="Drop-down KDE terminal emulator"
url="https://www.kde.org/applications/system/yakuake/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev karchive-dev kconfig-dev
	kcoreaddons-dev kcrash-dev kdbusaddons-dev kglobalaccel-dev ki18n-dev
	kiconthemes-dev kio-dev knewstuff-dev knotifications-dev kparts-dev
	knotifyconfig-dev kwidgetsaddons-dev kwindowsystem-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/yakuake/$pkgver/src/yakuake-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="17640590bb87b7c27e1e336cb82141b986103af95cfae2da129b69537d89a78eb0e21a6f5fb9ab26ed9e572f9edad055264f642afee6012e1a5a5c55d9dc2d22  yakuake-3.0.5.tar.xz"
