# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kolf
pkgver=18.12.2
pkgrel=0
pkgdesc="2D miniature golf game from KDE"
url="https://www.kde.org/applications/games/kolf/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kcompletion-dev
	kconfig-dev kconfigwidgets-dev kcoreaddons-dev kcrash-dev
	kdbusaddons-dev kdelibs4support-dev kdoctools-dev ki18n-dev kio-dev
	kwidgetsaddons-dev kxmlgui-dev libkdegames-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kolf-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="4ca61873bb5a4c765be7098197ac7d22d0c1452ed2404b90a10c26eaac7440612dda1ba6ca49f00fcecefb7071f19601853d3ccfdbe46e538783a8e85fa38fd0  kolf-18.12.2.tar.xz"
