# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=flac
pkgver=1.3.2
pkgrel=2
pkgdesc="Free Lossless Audio Codec"
url="https://xiph.org/flac/"
arch="all"
options="!checkroot"
license="BSD-3-Clause AND GPL-2.0+"
subpackages="$pkgname-dev $pkgname-doc"
depends=
makedepends="libogg-dev"
source="https://downloads.xiph.org/releases/flac/flac-${pkgver}.tar.xz
	"

build() {
	cd "$builddir"

	local _arch_conf
	case "${CARCH}" in
		ppc*)	_arch_conf="--enable-altivec" ;;
		x86_64)	_arch_conf="--enable-sse" ;;
		x86)	_arch_conf="--disable-sse" ;;
	esac

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--enable-shared \
		--enable-ogg \
		--disable-rpath \
		--with-pic \
		$_arch_conf
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install

	install -Dm0644 COPYING.Xiph \
		"$pkgdir"/usr/share/licenses/$pkgname/COPYING.Xiph
}

sha512sums="63910e8ebbe508316d446ffc9eb6d02efbd5f47d29d2ea7864da9371843c8e671854db6e89ba043fe08aef1845b8ece70db80f1cce853f591ca30d56ef7c3a15  flac-1.3.2.tar.xz"
