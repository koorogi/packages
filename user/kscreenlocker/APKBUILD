# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kscreenlocker
pkgver=5.12.7
pkgrel=0
pkgdesc="Secure X11 screen locker"
url="https://www.kde.org/"
arch="all"
options="!check"  # requires loginctl
license="(GPL-2.0-only OR GPL-3.0-only) AND GPL-2.0+"
depends="linux-pam"
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 libx11-dev
	libxcb-dev libxcursor-dev libxext-dev libxi-dev linux-pam-dev
	qt5-qtdeclarative-dev kcmutils-dev kcrash-dev kdeclarative-dev
	kglobalaccel-dev ki18n-dev kidletime-dev knotifications-dev
	ktextwidgets-dev kwayland-dev kwindowsystem-dev kxmlgui-dev solid-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/kscreenlocker-$pkgver.tar.xz
	kde.pam
	kde-np.pam"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
	install -D -m644 "$srcdir"/kde.pam "$pkgdir"/etc/pam.d/kde
	install -m644 "$srcdir"/kde-np.pam "$pkgdir"/etc/pam.d/kde-np
}

sha512sums="76554e8ad6872705e903bda5f55f650d859b282b400ca739f346e392ce7961891057c8a8a43b5dcb85c83860e4744252500c338e5d99fd67a33df529a7e040c6  kscreenlocker-5.12.7.tar.xz
56e87d02d75c4a8cc4ed183faed416fb4972e7f223b8759959c0f5da32e11e657907a1df279d62a44a6a174f5aca8b2ac66a5f3325c5deb92011bcf71eed74c3  kde.pam
565265485dd7466b77966d75a56766216b8bcc187c95a997e531e9481cf50ddbe576071eb0e334421202bcab19aa6de6b93e042447ca4797a24bf97e1d053ffd  kde-np.pam"
