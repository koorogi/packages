# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=alkimia
pkgver=7.0.2
pkgrel=0
pkgdesc="Library for common financial functionality"
url="https://community.kde.org/Alkimia/libalkimia"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="gmp-dev qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules"
subpackages="$pkgname-dev"
source="https://download.kde.org/stable/alkimia/$pkgver/alkimia-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="275ea48cd33caed9393a170efcc6053a581c1cc000862dd73a63bdb6de3e3c3ad2e492901a859e517033e1239e297a43088580553efe32f1d99a85051f5af3e4  alkimia-7.0.2.tar.xz"
