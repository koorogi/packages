# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=baloo-widgets
pkgver=18.12.1
pkgrel=0
pkgdesc="Widgets that utilise the Baloo desktop indexing engine"
url="https://www.KDE.org/"
arch="all"
options="!check"  # Requires /etc/fstab to exist, and udisks2 to be running.
license="GPL-2.0+"
depends=""
depends_dev="qt5-qtbase-dev baloo-dev kio-dev"
makedepends="$depends_dev cmake extra-cmake-modules kconfig-dev
	kfilemetadata-dev ki18n-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/baloo-widgets-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="449dfb83d9787e5a65dc59c7927a0d8586376933bf61b658d8338d4e9cd101c2b2d51ac0bc8f1a9fcf4f8827ec1cb557617f91a6ccceb351383320f13c61c2b1  baloo-widgets-18.12.1.tar.xz"
