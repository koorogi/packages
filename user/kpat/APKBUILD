# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kpat
pkgver=18.12.2
pkgrel=0
pkgdesc="Collection of card games for KDE"
url="https://games.kde.org/game.php?game=kpat"
arch="all"
license="GPL-2.0-only"
depends="libkdegames-carddecks"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev kconfig-dev
	kcompletion-dev kconfigwidgets-dev kcoreaddons-dev kcrash-dev ki18n-dev
	kdbusaddons-dev kdoctools-dev kguiaddons-dev kio-dev knewstuff-dev
	kwidgetsaddons-dev kxmlgui-dev libkdegames-dev shared-mime-info
	freecell-solver-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kpat-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="1eddfb90fb130ea1f70b21788e7f8ac4c26cd6c96dbb7de47227770593317935d57ae9e12cdc9bfb056a22af91fed24c626441408fba6b3c89c42ca6deda9d4a  kpat-18.12.2.tar.xz"
