# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Adélie Perl Team <adelie-perl@lists.adelielinux.org>
pkgname=perl-test-command
_pkgreal=Test-Command
pkgver=0.11
pkgrel=2
pkgdesc="Test routines for external commands in Perl"
url="https://metacpan.org/release/Test-Command"
arch="noarch"
license="GPL-2.0-only OR Artistic-1.0-Perl"
cpandepends=""
cpanmakedepends="perl-module-build"
depends="$cpandepends"
makedepends="perl-dev $cpanmakedepends"
subpackages="$pkgname-doc"
source="https://search.cpan.org/CPAN/authors/id/D/DA/DANBOO/$_pkgreal-$pkgver.tar.gz"
builddir="$srcdir/$_pkgreal-$pkgver"

prepare() {
	cd "$builddir"
	if [ -e Build.PL ]; then
		perl Build.PL installdirs=vendor
	else
		PERL_MM_USE_DEFAULT=1 perl Makefile.PL INSTALLDIRS=vendor
	fi
}

build() {
	cd "$builddir"
	export CFLAGS=`perl -MConfig -E 'say $Config{ccflags}'`
	./Build
}

check() {
	cd "$builddir"
	./Build test
}

package() {
	cd "$builddir"
	./Build install destdir="$pkgdir"
	find "$pkgdir" \( -name perllocal.pod -o -name .packlist \) -delete
}

sha512sums="79a8f41132b965ad4f6e8b4d97f8fb6181a1e394bcf8825abda2c8ee12dd5f6ef8d7c69df84d306c3841bb516213742c4a0a43c2f3d6b39ce6e163d6d77f45f8  Test-Command-0.11.tar.gz"
