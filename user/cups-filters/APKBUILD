# Maintainer: Max Rees <maxcrees@me.com>
pkgname=cups-filters
pkgver=1.21.6
pkgrel=0
pkgdesc="OpenPrinting CUPS filters and backends"
url="https://wiki.linuxfoundation.org/openprinting/cups-filters"
arch="all"
license="GPL-2.0-only AND GPL-2.0+ AND GPL-3.0-only AND MIT"
depends="gnu-ghostscript poppler-utils bc ttf-freefont"
makedepends="cups-dev libjpeg-turbo-dev poppler-dev zlib-dev libpng-dev
	tiff-dev lcms2-dev freetype-dev fontconfig-dev qpdf-dev dbus-dev linux-headers
	coreutils gnutls-dev python3"
checkdepends="ttf-dejavu"
subpackages="$pkgname-dev $pkgname-doc $pkgname-libs"
source="https://www.openprinting.org/download/cups-filters/cups-filters-$pkgver.tar.xz"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--disable-static \
		--with-pdftops=pdftops \
		--with-shell=/bin/sh \
		--without-rcdir \
		--without-rclevels \
		--disable-avahi \
		--disable-mutool \
		--with-test-font-path='/usr/share/fonts/ttf-dejavu/DejaVuSans.ttf'
	# workaround parallel build issue by building libcupsfilters.la first
	make libcupsfilters.la && make libfontembed.la && make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make -j1 DESTDIR="$pkgdir" install
	# the pdf.utf-8 symlink isn't quite good enough
	cd "$pkgdir"/usr/share/cups/charsets && \
	ln -s pdf.utf-8.simple pdf.UTF-8
}

dev() {
	default_dev
	# cupsfilters.drv needs pcl.h
	install -Dm644 "$builddir"/filter/pcl.h \
		"$pkgdir"/usr/share/cups/ppdc/pcl.h
}

libs() {
	pkgdesc="OpenPrinting CUPS filters and backends - cupsfilters and fontembed libraries"
	install -d "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/lib*.so.* "$subpkgdir"/usr/lib/
}

sha512sums="804250745ac710706ff1bfa6e161c0b1a8a65a74850a76a311b7614694a7e5d07f01dfd15f277ad79ed7fe1e84ea680bab1643e0b82cefa3e26603fa2eea935a  cups-filters-1.21.6.tar.xz"
