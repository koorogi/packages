# Contributor: William Pitcock <nenolod@dereferenced.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gpgme
pkgver=1.12.0
pkgrel=0
pkgdesc="GnuPG Made Easy"
url="https://www.gnupg.org/related_software/gpgme/"
arch="all"
# gpgme-tool: GPL3; lib is mixture of the rest
license="(LGPL-3.0+ OR GPL-2.0+) AND LGPL-2.1+ AND MIT AND GPL-3.0+"
depends="gnupg"
depends_dev="libgpg-error-dev libassuan-dev qt5-qtbase-dev"
makedepends="$depends_dev doxygen"
subpackages="$pkgname-dev $pkgname-doc gpgmepp qgpgme"
source="ftp://ftp.gnupg.org/gcrypt/$pkgname/$pkgname-$pkgver.tar.bz2"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

qgpgme() {
	pkgdesc="$pkgdesc (Qt 5 library)"
	mkdir -p "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libqgpgme.so* "$subpkgdir"/usr/lib/
}

check() {
	cd "$builddir"
	make check
}

gpgmepp() {
	pkgdesc="C++ bindings for GPGME"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgpgmepp.so.* "$subpkgdir"/usr/lib/
}


sha512sums="c228b3df28377df882be536ada56dc9c73150048a58e591aa4495f89c854af95820152cd60139840f994c249e9c7df50d8b89eb9d6dc4ce02aa80bbfebcdd014  gpgme-1.12.0.tar.bz2"
