# Contributor: Michael Mason <ms13sp@gmail.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=screen
pkgver=4.6.2
pkgrel=0
pkgdesc="A window manager that multiplexes a physical terminal"
url="http://ftp.gnu.org/gnu/screen/"
arch="all"
license="GPL-3.0+"
options="!check"  # No test suite.
makedepends="ncurses-dev utmps-dev"
subpackages="$pkgname-doc"
source="http://ftp.gnu.org/gnu/$pkgname/$pkgname-$pkgver.tar.gz
	utmpx.patch"

build() {
	cd "$builddir"
	LIBS="-lutmps -lskarnet" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--enable-colors256
	make
}

package() {
	cd "$builddir"
	make -j1 DESTDIR="$pkgdir" install

	# remove suid root
	find "$pkgdir" -type f -perm -u+s -print0 \
		| xargs -0 chmod -s

	install -Dm644 etc/etcscreenrc "$pkgdir"/etc/screenrc
	install -Dm644 etc/screenrc "$pkgdir"/etc/skel/.screenrc
}

sha512sums="224bd16ad5ae501d1b8bb7d2ba9cc19e6a0743de5a5b320109c2f6bf3b1ca564cc7094ed9211be13733d9d769cde77d13fe236341d448cad0518038ab1e85c99  screen-4.6.2.tar.gz
82aca3e16c8cd7a3029d3b589ff2dd3471708d6287979ebb5cfdaedbd1f3012c0cd660b131e0cbe142b99786e49ef8b24c63159523d870e95ccf71ec94b82634  utmpx.patch"
