# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=physfs
pkgver=3.0.1
pkgrel=0
pkgdesc="Library to access various archives for game engines"
url="https://www.icculus.org/physfs"
arch="all"
license="Zlib"
depends=""
makedepends=""
subpackages="$pkgname-dev $pkgname-tools"
source="https://icculus.org/physfs/downloads/physfs-$pkgver.tar.bz2"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

tools() {
	pkgdesc="Tool to explore game archives from PhysicsFS"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

sha512sums="ddf3b075ccb506da5e9a1ce96001be402752b9b777c2e816a85d48aff3626ff0886ea43eb07bd300fe3a9f59b9a002f54d822c51d483a4ee94b38378534c1879  physfs-3.0.1.tar.bz2"
