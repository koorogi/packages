# Contributor: Mika Havela <mika.havela@gmail.com>
# Maintainer: 
pkgname=fontconfig
pkgver=2.13.1
pkgrel=0
pkgdesc="Library for configuring and customizing font access"
url="https://www.freedesktop.org/wiki/Software/fontconfig/"
arch="all"
options="!check"  # Fails test with fixed fonts
license="MIT"
depends=""
makedepends="freetype-dev expat-dev python3-dev gperf util-linux-dev"
triggers="$pkgname.trigger=/usr/share/fonts/*"
subpackages="$pkgname-doc $pkgname-dev $pkgname-lang"
source="https://www.freedesktop.org/software/fontconfig/release/${pkgname}-${pkgver}.tar.gz"

build() {
	cd "$builddir"
	# regenerate hash functions
	rm -f src/fcobjshash.h

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--enable-static \
		--disable-docs
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make -j1 DESTDIR="$pkgdir" install

	install -m644 -D COPYING "$pkgdir"/usr/share/licenses/"${pkgname}"/COPYING
}

sha512sums="830df32e944ee21ad02a9df04787b9902af36ffc13913524acef6e38799a38c5df7a6e407cc0ff9c24455520549d53b3d85d22642a229ac654dc9269926f130b  fontconfig-2.13.1.tar.gz"
