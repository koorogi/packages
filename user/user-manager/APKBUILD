# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=user-manager
pkgver=5.12.7
pkgrel=0
pkgdesc="Manage user accounts from KDE"
url="https://www.KDE.org/"
arch="all"
license="GPL-2.0+ AND LGPL-2.0+ AND GPL-2.0-only"
depends="accountsservice"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kauth-dev kcmutils-dev
	kconfig-dev kconfigwidgets-dev kcoreaddons-dev ki18n-dev kiconthemes-dev
	kio-dev kwidgetsaddons-dev libpwquality-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/user-manager-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="286b4adc7ff01c25f1f32bc64e9f119aa693d74e0468e575c792d5357df6066c18de233eb78acd5ed4288518902461ffa7408af8d559e8b5f4081893bdec7f8f  user-manager-5.12.7.tar.xz"
