# Contributor: Sergey Lukin <sergej.lukin@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=icu
pkgver=63.1

# convert x.y.z to x_y_z
_ver=${pkgver//./_}

pkgrel=1
pkgdesc="International Components for Unicode library"
url="http://www.icu-project.org/"
arch="all"
license="ICU"
subpackages="$pkgname-static $pkgname-dev $pkgname-doc $pkgname-libs"
depends=""
checkdepends="diffutils"
makedepends=""
source="http://download.icu-project.org/files/icu4c/${pkgver}/${pkgname}4c-$_ver-src.tgz
	icu-60.2-always-use-utf8.patch
	checkimpl.patch
	"

# secfixes:
#   57.1-r1:
#     - CVE-2016-6293
#   58.1-r1:
#     - CVE-2016-7415
#   58.2-r2:
#     - CVE-2017-7867
#     - CVE-2017-7868
builddir="$srcdir"/icu/source

prepare() {
	cd "$builddir"
	default_prepare
	update_config_sub

	# strtod_l() is not supported by musl; also xlocale.h is missing
	# It is not possible to disable its use via configure switches or env vars
	# so monkey patching is needed. Idea was stollen from openembedded
	# https://github.com/openembedded/openembedded-core/blob/master/meta/recipes-support/icu/icu.inc#L30
	sed -i -e 's,DU_HAVE_STRTOD_L=1,DU_HAVE_STRTOD_L=0,' configure.ac
	sed -i -e 's,DU_HAVE_STRTOD_L=1,DU_HAVE_STRTOD_L=0,' configure

	local x
	# https://bugs.icu-project.org/trac/ticket/6102
	for x in ARFLAGS CFLAGS CPPFLAGS CXXFLAGS FFLAGS LDFLAGS; do
		sed -i -e "/^${x} =.*/s:@${x}@::" "config/Makefile.inc.in"
	done
}

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--enable-shared \
		--enable-static \
		--with-data-packaging=library \
		--disable-samples \
		--mandir=/usr/share/man
	make
}

check() {
	# armhf tests fail with gensprep: Bus error
	[ "$CARCH" != armhf ] || return 0
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make -j1 DESTDIR="$pkgdir" install
	chmod +x "$pkgdir"/usr/bin/icu-config
	install -Dm644 "$srcdir"/icu/license.html \
		"$pkgdir"/usr/share/licenses/icu/license.html
}

static() {
	pkgdesc="$pkgdesc (Static libraries)"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/*.a "$subpkgdir"/usr/lib/
}

sha512sums="9ab407ed840a00cdda7470dcc4c40299a125ad246ae4d019c4b1ede54781157fd63af015a8228cd95dbc47e4d15a0932b2c657489046a19788e5e8266eac079c  icu4c-63_1-src.tgz
f86c62422f38f6485c58d4766e629bab69e4b0e00fa910854e40e7db1ace299152eaefa99ae2fbab7465e65d3156cbea7124612defa60680db58ab5c34d6262f  icu-60.2-always-use-utf8.patch
af27a474af041a6ac522901a635c3f328dee5f2b8e42d1229970908c740cd2b97fc06e5432541773d7c80339382f75d795911540f844b6d89ec0ee99d4fa6ff9  checkimpl.patch"
