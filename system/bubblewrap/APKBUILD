# Contributor: Timo Teräs <timo.teras@iki.fi>
# Maintainer: 
pkgname=bubblewrap
pkgver=0.3.1
pkgrel=0
pkgdesc="Unprivileged sandboxing tool"
url="https://github.com/projectatomic/bubblewrap"
arch="all"
options="!check suid"  # ?
license="LGPL-2.0+"
makedepends="autoconf automake libcap-dev docbook-xsl"
subpackages="$pkgname-doc $pkgname-bash-completion:bashcomp:noarch"
source="bubblewrap-$pkgver.tar.gz::https://github.com/projectatomic/bubblewrap/archive/v$pkgver.tar.gz
	realpath-workaround.patch musl-fixes.patch"

prepare() {
	cd "$builddir"
	NOCONFIGURE=1 ./autogen.sh
	default_prepare
}

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--with-priv-mode=setuid
	make
}

package() {
	cd "$builddir"
	make install DESTDIR="$pkgdir"
}

bashcomp() {
	pkgdesc="Bash completions for $pkgname"
	depends=""
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"

	mkdir -p "$subpkgdir"/usr/share/
	mv "$pkgdir"/usr/share/bash-completion/ "$subpkgdir"/usr/share/
}

sha512sums="fbc44976f53fdf8913b94c57d1f26a3b87c773e86a289e58fd3d7b1c4ea7f33c862f1a38a4f791315358990928768a68334f0a171302c18a16c7e2e9f1a146dd  bubblewrap-0.3.1.tar.gz
400a0446670ebf80f16739f1a7a2878aadc3099424f957ba09ec3df780506c23a11368f0578c9e352d7ca6473fa713df826fad7a20c50338aa5f9fa9ac6b84a4  realpath-workaround.patch
f59cda3b09dd99db9ca6d97099a15bb2523e054063d677502317ae3165ba2e32105a0ae8f877afc3827bd28d093c9d9d413270f4c87d9fe5f26f3eee670d916e  musl-fixes.patch"
