# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=attr
pkgver=2.4.48
pkgrel=0
pkgdesc="Utilities for managing filesystem extended attributes"
url="https://savannah.nongnu.org/projects/attr"
arch="all"
license="GPL-2.0+ AND LGPL-2.1+"
options="!checkroot"
depends=""
makedepends="libtool autoconf automake bash gettext-tiny gettext-tiny-dev"
checkdepends="perl"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang libattr"
source="http://download.savannah.nongnu.org/releases/attr/attr-$pkgver.tar.gz
	test-runner-musl.patch
	test-runner-perl.patch
	"

prepare() {
	cd "$builddir"
	default_prepare
}

build() {
	cd "$builddir"

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--exec-prefix=/ \
		--sbindir=/sbin \
		--libdir=/lib \
		--includedir=/usr/include \
		--mandir=/usr/share/man \
		--datadir=/usr/share
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make -j1 DESTDIR="$pkgdir" install
}

libattr() {
	pkgdesc="Dynamic library for extended attribute support"
	replaces="attr"
	mkdir -p "$subpkgdir"/lib
	mv "$pkgdir"/lib/lib*.so.* "$subpkgdir"/lib/
}

sha512sums="75f870a0e6e19b8975f3fdceee786fbaff3eadaa9ab9af01996ffa8e50fe5b2bba6e4c22c44a6722d11b55feb9e89895d0151d6811c1d2b475ef4ed145f0c923  attr-2.4.48.tar.gz
da4b903ae0ba1c72bae60405745c1135d1c3c1cefd7525fca296f8dc7dac1e60e48eeba0ba80fddb035b24b847b00c5a9926d0d586c5d7989d0428e458d977d3  test-runner-musl.patch
d10821cc73751171c6b9cc4172cf4c85be9b6e154782090a262a16fd69172a291c5d5c94587aebcf5b5d1e02c27769245d88f0aa86478193cf1a277ac7f4f18e  test-runner-perl.patch"
