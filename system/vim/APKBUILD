# Contributor: Sergei Lukin <sergej.lukin@gmail.com>
# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=vim
pkgver=8.1.0952
pkgrel=0
pkgdesc="advanced text editor"
url="http://www.vim.org"
arch="all"
options="!check"  # requires controlling TTY, and fails with musl locales
license="Vim"
depends=""
makedepends_host="acl-dev ncurses-dev"
[ "$CBUILD" != "$CHOST" ] || makedepends_host="$makedepends_host perl-dev python3-dev"
subpackages="$pkgname-doc ${pkgname}diff::noarch"
source="$pkgname-$pkgver.tar.gz::https://github.com/$pkgname/$pkgname/archive/v$pkgver.tar.gz
	vimrc
	no-default-mouse.patch
	"
builddir="$srcdir/$pkgname-$pkgver"

# secfixes:
#   8.0.0329-r0:
#     - CVE-2017-5953
#   8.0.0056-r0:
#     - CVE-2016-1248

prepare() {
	cd "$builddir"
	# Read vimrc from /etc/vim
	echo '#define SYS_VIMRC_FILE "/etc/vim/vimrc"' >> src/feature.h
}

build() {
	local _onlynative
	cd "$builddir"
	[ "$CBUILD" != "$CHOST" ] || _onlynative="--enable-perlinterp=dynamic --enable-python3interp=dynamic"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		$_onlynative \
		--without-x \
		--enable-acl \
		--enable-nls \
		--enable-multibyte \
		--enable-gui=no \
		--with-compiledby="Adélie Linux" \
		vim_cv_toupper_broken=no \
		vim_cv_terminfo=yes \
		vim_cv_tgent=zero \
		vim_cv_tty_group=world \
		vim_cv_getcwd_broken=no \
		vim_cv_stat_ignores_slash=no \
		vim_cv_memmove_handles_overlap=yes \
		STRIP=:
	make
}

package() {
	cd "$builddir"
	make -j1 DESTDIR="$pkgdir/" install

	install -Dm644 runtime/doc/uganda.txt \
		"$pkgdir/usr/share/licenses/$pkgname/LICENSE"
	install -Dm644 "$srcdir"/vimrc "$pkgdir"/etc/vim/vimrc

	# the following properly belong to gvim.
	rm -r "$pkgdir"/usr/share/applications
	rm "$pkgdir"/usr/share/vim/vim81/gvimrc_example.vim
	rm -r "$pkgdir"/usr/share/icons
}

vimdiff() {
	pkgdesc="view file diffs in vim"
	depends="diffutils"

	install -d "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/vimdiff "$subpkgdir"/usr/bin
}

sha512sums="485d839901b3d1fc8efcc035991d5f34849f6d05d9c556dcf5b2918cd01e7f7b490cd0a7eb976c7577adb41962dceea4552474103f0b3c7807311d07f8158bd1  vim-8.1.0952.tar.gz
12ee3f96c94d74215159fba379ed61907ec5982a9f1643575dcb7c3d5e30824665d683de95f97b5067718b3f2a1238fb7534a70803bc170614498ad026f352d8  vimrc
16026a3ed3e080b3f8281948579ab678e9acd724ad594463279712fbf1024bcd923155a133bd08118848d2c6cdf11c69489d85b1c7438f60b4c279094714d181  no-default-mouse.patch"
