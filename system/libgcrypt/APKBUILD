# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libgcrypt
pkgver=1.8.4
pkgrel=0
pkgdesc="GnuPG cryptography library"
url="http://www.gnupg.org"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="libgpg-error-dev"
makedepends="$depends_dev texinfo"
subpackages="$pkgname-dev $pkgname-doc"
source="https://www.gnupg.org/ftp/gcrypt/libgcrypt/$pkgname-$pkgver.tar.bz2"

build() {
	cd "$builddir"

	local _arch_configure=
	case "$CARCH" in
	arm*)
		# disable arm assembly for now as it produces TEXTRELs
		export gcry_cv_gcc_arm_platform_as_ok=no
		;;
	x86 | pmmx | x86_64)
		_arch_configure="--enable-padlock-support"
		;;
	esac

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-static \
		$_arch_configure

	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make -j1 DESTDIR="$pkgdir" install
	rm -f ${pkgdir}/usr/share/info/dir
}

sha512sums="b831fc337eb14806897e224b0d1e78d1f8e9db91dffa818a015a4aa104144e2d971e5a855904907ee2bb9990a9d526de32d6787e1cae98e28a65c3258b2b1ea1  libgcrypt-1.8.4.tar.bz2"
