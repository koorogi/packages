# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Contributor: Michael Mason <ms13sp@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=coreutils
pkgver=8.30
pkgrel=0
pkgdesc="Basic file, shell, and text manipulation utilities"
url="http://www.gnu.org/software/coreutils/"
arch="all"
license="GPL-3.0+"
options="!checkroot"
makedepends="bash acl-dev attr-dev"
[ ${CBUILD} == ${CHOST} ] && makedepends="$makedepends perl gettext-tiny
						gettext-tiny-dev utmps-dev"
subpackages="$pkgname-doc"
[ ${CBUILD} == ${CHOST} ] && subpackages="$subpackages $pkgname-lang"
install="$pkgname.post-deinstall"
source="http://ftp.gnu.org/gnu/coreutils/$pkgname-$pkgver.tar.xz
	disable-csplit-io-err-test.patch
	disable-mbrtowc-test.patch
	localename-test-fix.patch
	"
[ ${CBUILD} != ${CHOST} ] && source="$source
	051_all_coreutils-mangen.patch
	"

build() {
	cd "$builddir"
	LIBS="-lutmps -lskarnet" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--enable-nls \
		--without-gmp \
		--enable-no-install-program=hostname,su,kill,uptime \
		--with-packager="Adélie" \
		--with-packager-bug-reports="https://bts.adelielinux.org/" \
		--enable-single-binary=symlinks

	if [ ${CBUILD} != ${CHOST} ]; then
		set -- man/*.x
		touch ${@/%x/1}
	fi
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install

	rm -rf "$pkgdir"/usr/lib/charset.alias
	rmdir -p "$pkgdir"/usr/lib 2>/dev/null || true

	install -d "$pkgdir"/bin "$pkgdir"/usr/sbin
	cd "$pkgdir"/usr/bin/

	# We want to put these in /bin to match busybox (?)
	local busybox_bin="base64 cat chgrp chmod chown cp date dd df echo
		false ln ls mkdir mknod mktemp mv nice printenv pwd rm rmdir
		sleep stat stty sync touch true uname"

	for i in $busybox_bin; do
		rm "$pkgdir"/usr/bin/$i
		ln -s ../usr/bin/coreutils "$pkgdir"/bin/$i
	done

	rm "$pkgdir"/usr/bin/chroot
	ln -s ../bin/coreutils "$pkgdir"/usr/sbin/chroot
}

sha512sums="25bc132c0d89ce71c33e417f04649c9fcfce6c5ef8b19f093b2e9e2851bfde9b5a31e20499d9c427332228ba54b88d445ddb445551e1944bb8f5cbff5ffa4eda  coreutils-8.30.tar.xz
bd8b5cecba59eba30458c8afa895877d39a987b989bc06b70cd6836e0ef684677aaadcb4949b58a713779fe1df1e15e3903e9be14e306671b86b69018b75de8b  disable-csplit-io-err-test.patch
532ef8210261437855ba3e2fc72ec57b0e0252b2b0f7817990c9629613ece2cdd86cf30aa049f3cff6fbda12ed3bb9a0db3a3642475378ae44ce319bdf005d9b  disable-mbrtowc-test.patch
86944e9ea5493c5b21ab62562a63aa134952738297b21cfb2e8f751db4a695ff25ee1592eb231ed1bb80999bc31f7504c7feffde2e93b4e068c05c44ac83cbdf  localename-test-fix.patch"

[ ${CBUILD} != ${CHOST} ] && sha512sums="$sha512sums
bae804f057252c25452ac178c545dc2c4b4775cbfbdcfd4775edd1a4ed6507882bfac39e2b11ad01b74230ca48d761bf62f11bde5bcbc35a6e5a61cbe4a46e36  051_all_coreutils-mangen.patch
"
return 0
