# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Adélie Perl Team <adelie-perl@lists.adelielinux.org>
# Proxy-For: solar
pkgname=perl
pkgver=5.26.3
pkgrel=0
pkgdesc="Larry Wall's Practical Extraction and Report Language"
url="http://www.perl.org/"
arch="all"
license="Artistic-1.0-Perl GPL-2.0"
# remove !check when tests pass.
options="!check !fhs"
depends=""
makedepends="bzip2-dev zlib-dev"
subpackages="$pkgname-doc $pkgname-dev"
source="http://www.cpan.org/src/5.0/perl-$pkgver.tar.gz
	"

# secfixes:
#   5.26.3-r0:
#   - CVE-2018-12015
#   - CVE-2018-18311
#   - CVE-2018-18312
#   - CVE-2018-18313
#   - CVE-2018-18314
#   5.26.1-r0:
#   - CVE-2017-12837
#   - CVE-2017-12883

builddir="$srcdir/$pkgname-$pkgver"

_privlib=/usr/share/perl5/core_perl
_archlib=/usr/lib/perl5/core_perl

prepare() {
	chmod +w "$builddir"/*.c
	default_prepare
	cd "$builddir"
	sed	-e 's/less -R/less/g' \
		-e 's/libswanted="\(.*\) nsl\(.*\)"/libswanted="\1\2"/g' \
		-i ./Configure ||

	# Ensure that we never accidentally bundle zlib or bzip2
	rm -rf cpan/Compress-Raw-Zlib/zlib-src
	rm -rf cpan/Compress-Raw-Bzip2/bzip2-src
	sed -i '/\(bzip2\|zlib\)-src/d' MANIFEST

}

build() {
	cd "$builddir"
	BUILD_ZLIB=0
	BUILD_BZIP2=0
	BZIP2_LIB=/usr/lib
	BZIP2_INCLUDE=/usr/inculde

	export BUILD_ZLIB BUILD_BZIP2 BZIP2_LIB BZIP2_INCLUDE
	./Configure -des \
		-Dcccdlflags='-fPIC' \
		-Dcccdlflags='-fPIC' \
		-Dccdlflags='-rdynamic' \
		-Dprefix=/usr \
		-Dprivlib=$_privlib \
		-Darchlib=$_archlib \
		-Dvendorprefix=/usr \
		-Dvendorlib=/usr/share/perl5/vendor_perl \
		-Dvendorarch=/usr/lib/perl5/vendor_perl \
		-Dsiteprefix=/usr/local \
		-Dsitelib=/usr/local/share/perl5/site_perl \
		-Dsitearch=/usr/local/lib/perl5/site_perl \
		-Dlocincpth=' ' \
		-Doptimize="$CFLAGS" \
		-Duselargefiles \
		-Dusethreads \
		-Duseshrplib \
		-Dd_semctl_semun \
		-Dman1dir=/usr/share/man/man1 \
		-Dman3dir=/usr/share/man/man3 \
		-Dinstallman1dir=/usr/share/man/man1 \
		-Dinstallman3dir=/usr/share/man/man3 \
		-Dman1ext='1' \
		-Dman3ext='3pm' \
		-Dcf_by='Adelie' \
		-Ud_csh \
		-Dusenm
	make libperl.so && make
}

check() {
	cd "$builddir"
	make test
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
	if [ -n "$(find $pkgdir/usr/local -type f)" ]; then
		error "files found under /usr/local"
	fi
	find "$pkgdir" -name '.*' -delete
}

dev() {
	mkdir -p "$subpkgdir"/usr/bin "$subpkgdir"/$_archlib "$subpkgdir"/$_privlib
	mv "$pkgdir"/$_archlib/Devel "$subpkgdir"/$_archlib/
	mv "$pkgdir"/$_privlib/Encode "$subpkgdir"/$_privlib/

	mv "$pkgdir"/usr/bin/h2xs \
		"$pkgdir"/usr/bin/perlivp \
		"$pkgdir"/usr/bin/enc2xs \
		"$pkgdir"/usr/bin/xsubpp \
		"$subpkgdir"/usr/bin/

	default_dev
}

sha512sums="03914ed51163c998a6afa45610a13cf50124a2c68d291c344b0d52fa15c27fc5d5d4f5dc117516078a03dfd51250097b87c8d5e2b17c7858a4c8c536aecd05af  perl-5.26.3.tar.gz"
