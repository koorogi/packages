# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: 
pkgname=pcre2
pkgver=10.32
pkgrel=0
pkgdesc="Perl-compatible regular expression library"
url="http://pcre.sourceforge.net/"
arch="all"
license="BSD-3-Clause"
depends=""
depends_dev="libedit-dev zlib-dev"
makedepends="$depends_dev paxmark"
subpackages="$pkgname-dev $pkgname-doc $pkgname-tools
	libpcre2-16:_libpcre libpcre2-32:_libpcre"
source="ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/$pkgname-$pkgver.tar.gz"
builddir="$srcdir/$pkgname-$pkgver"

case "$CARCH" in
	s390x) _enable_jit="";;
	ppc64) _enable_jit="";;
	pmmx) _enable_jit="";;  # maybe someday fix sse2 detection
	*) _enable_jit="--enable-jit";;
esac

build() {
	cd "$builddir"

	# Note: Forced -O3 is recommended (needed?) for Julia.
	./configure \
		CFLAGS="$CFLAGS -O3" \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--docdir=/usr/share/doc/$pkgname-$pkgver \
		--htmldir=/usr/share/doc/$pkgname-$pkgver/html \
		--enable-pcre2-16 \
		--enable-pcre2-32 \
		--enable-pcre2grep-libz \
		--enable-pcre2test-libedit \
		--with-match-limit-recursion=8192 \
		$_enable_jit
	make
}

check() {
	cd "$builddir"

	./RunTest
	[ ! -n "$_enable_jit" ] || ./pcre2_jit_test
}

package() {
	cd "$builddir"

	make DESTDIR="$pkgdir" install
}

_libpcre() {
	local bits="${subpkgname##*-}"
	pkgdesc="PCRE2 with $bits bit character support"

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libpcre2-$bits.so* "$subpkgdir"/usr/lib/
}

tools() {
	pkgdesc="Auxiliary utilities for PCRE2"

	mkdir -p "$subpkgdir"/usr/
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

sha512sums="b0a247dc6411546f231f7588b979bc89f4c6d498a27232fb1e883d5222b9275d7b87c6afc0c67f11a453b35238dca472d14246e8bcb38df70f3701f9f3e8030f  pcre2-10.32.tar.gz"
