# Contributor: Laurent Bercot <ska-adelie@skarnet.org>
# Maintainer: Laurent Bercot <ska-adelie@skarnet.org>
pkgname=utmps
pkgver=0.0.2.0
pkgrel=2
pkgdesc="A secure utmp/wtmp implementation"
url="https://skarnet.org/software/$pkgname/"
arch="all"
options="!check"  # No test suite
license="ISC"
depends="execline s6"
depends_dev="skalibs-dev"
makedepends="skalibs-dev>=2.7"
subpackages="$pkgname-dev $pkgname-doc"
install="$pkgname.post-upgrade"
source="https://skarnet.org/software/$pkgname/$pkgname-$pkgver.tar.gz
	utmpd.run
	wtmpd.run"

build() {
	cd "$builddir"
	./configure \
		--enable-shared \
		--enable-static \
		--disable-allstatic \
		--libdir=/usr/lib \
		--libexecdir="/lib/$pkgname" \
		--with-dynlib=/lib
	make
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
        mkdir -p -m 0755 "$pkgdir/etc/s6/early-services/utmpd" "$pkgdir/etc/s6/early-services/wtmpd"
        cp -f "$srcdir/utmpd.run" "$pkgdir/etc/s6/early-services/utmpd/run"
	echo 3 > "$pkgdir/etc/s6/early-services/utmpd/notification-fd"
        cp -f "$srcdir/wtmpd.run" "$pkgdir/etc/s6/early-services/wtmpd/run"
	echo 3 > "$pkgdir/etc/s6/early-services/wtmpd/notification-fd"
        chmod 0755 "$pkgdir/etc/s6/early-services/utmpd/run" "$pkgdir/etc/s6/early-services/wtmpd/run"
}

doc() {
	default_doc
	mkdir -p "$subpkgdir/usr/share/doc"
	cp -a "$builddir/doc" "$subpkgdir/usr/share/doc/$pkgname"
}

sha512sums="5fffb86f68475f3eadf8da53e3ae40284040cceb644f453218a5fd9e1be081920b5c787283e2578a60922e2feb75c94a7430034a658f7399645fc3bcc9afa28f  utmps-0.0.2.0.tar.gz
525d43e3eced30c564e5390fc715b6caa1ae2b6515a9e3bf01263ff3fb9379bd6908ed302d0d50b6568ac36ed44d272dcc44a683f9ae34d586d8ad17023ed6b1  utmpd.run
93e4fae527ada9381e0b0a7ad5de9079e8d88959abd74fa5c0710c30c6153832abb010b57ddf83055ca34c032e7e5c9c1eedceb2f122a11ab20837ab66dcf5e2  wtmpd.run"
