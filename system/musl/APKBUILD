# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=musl
pkgver=1.1.21
pkgrel=3
pkgdesc="System library (libc) implementation"
url="http://www.musl-libc.org/"
arch="all"
options="!check"
license="MIT"
depends=""
makedepends=""
subpackages="$pkgname-dev"
case "$BOOTSTRAP" in
nocc)	pkgname="musl-dev"
	subpackages=""
	options="$options !dbg"
	builddir="$srcdir"/musl-$pkgver
	;;
nolibc) ;;
*)	subpackages="$subpackages $pkgname-utils"
	triggers="$pkgname-utils.trigger=/etc/ld.so.conf.d"
	;;
esac
source="http://www.musl-libc.org/releases/musl-$pkgver.tar.gz
	amalgamation.patch
	3001-make-real-lastlog-h.patch
	handle-aux-at_base.patch
	fgetspent_r.patch
	gettext-preserve-errno.patch

	ldconfig
	getent.c
	iconv.c
	"

# secfixes:
#   1.1.15-r4:
#     - CVE-2016-8859

build() {
	cd "$builddir"

	[ "$BOOTSTRAP" = "nocc" ] && return 0

	if [ "$BOOTSTRAP" != "nolibc" ]; then
		# getconf/getent/iconv
		local i
		for i in getent iconv ; do
			${CROSS_COMPILE}gcc $CPPFLAGS $CFLAGS "$srcdir"/$i.c -o $i
		done
	fi

	# note: not autotools
	LDFLAGS="$LDFLAGS -Wl,-soname,libc.musl-${CARCH}.so.1" \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var
	make
}

package() {
	cd "$builddir"

	if [ "$BOOTSTRAP" = "nocc" ]; then
		case "$CARCH" in
		aarch64*)	ARCH="aarch64" ;;
		arm*)		ARCH="arm" ;;
		x86)		ARCH="i386" ;;
		x86_64)		ARCH="x86_64" ;;
		ppc)		ARCH="powerpc" ;;
		ppc64*)		ARCH="powerpc64" ;;
		s390*)		ARCH="s390x" ;;
		mips64*)	ARCH="mips64" ;;
		mips*)		ARCH="mips" ;;
		m68k)		ARCH="m68k" ;;
		esac

		make ARCH="$ARCH" prefix=/usr DESTDIR="$pkgdir" install-headers
	else
		make DESTDIR="$pkgdir" install

		# make LDSO the be the real file, and libc the symlink
		local LDSO=$(make -f Makefile --eval "$(echo -e 'print-ldso:\n\t@echo $$(basename $(LDSO_PATHNAME))')" print-ldso)
		mv -f "$pkgdir"/usr/lib/libc.so "$pkgdir"/lib/"$LDSO"
		ln -sf "$LDSO" "$pkgdir"/lib/libc.musl-${CARCH}.so.1
		ln -sf ../../lib/"$LDSO" "$pkgdir"/usr/lib/libc.so
		mkdir -p "$pkgdir"/usr/bin
		ln -sf ../../lib/"$LDSO" "$pkgdir"/usr/bin/ldd
	fi

	rm "$pkgdir"/usr/include/utmp.h     # utmps
	rm "$pkgdir"/usr/include/utmpx.h    # utmps
}

dev() {
	provides="libc-dev=$pkgver-r$pkgrel"
	default_dev
}

utils() {
	depends="!uclibc-utils scanelf"
	provides="libc-utils=$pkgver-r$pkgrel"
	replaces="libiconv uclibc-utils"
	license="BSD-2-Clause AND GPL-2.0+"

	mkdir -p "$subpkgdir"/usr "$subpkgdir"/sbin
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/

	install -D \
		"$builddir"/getent \
		"$builddir"/iconv \
		"$subpkgdir"/usr/bin

	install -D -m755 "$srcdir"/ldconfig "$subpkgdir"/sbin
}

sha512sums="fa6c4cc012626c5e517e0e10926fc845e3aa5f863ffaceeb38ac5b9ce0af631a37f6b94f470997db09aa0d5e03f4f28a2db83484b0f98481bea2239c1989d363  musl-1.1.21.tar.gz
3162b778101a46d9fb3a38927e3c4969781ccdabb9bb72de10d48ea31691215ecba605faee945ed86144a7e31fc948c31c2fee2881dc07897be3e82b6c6ff1e6  amalgamation.patch
88ae443dbb8e0a4368235bdc3a1c5c7b718495afa75e06deb8e01becc76cb1f0d6964589e2204fc749c9c1b3190b8b9ac1ae2c0099cab8e2ce3ec877103d4332  3001-make-real-lastlog-h.patch
6a7ff16d95b5d1be77e0a0fbb245491817db192176496a57b22ab037637d97a185ea0b0d19da687da66c2a2f5578e4343d230f399d49fe377d8f008410974238  handle-aux-at_base.patch
ded41235148930f8cf781538f7d63ecb0c65ea4e8ce792565f3649ee2523592a76b2a166785f0b145fc79f5852fd1fb1729a7a09110b3b8f85cba3912e790807  fgetspent_r.patch
db180e437b8b7582e4d2baf06b592b88a9f6e5a8f18b7afa81d7a707240a774273778f8fec1c5cbea2a137e00cca49ff08fe762c871be20c70b50104b7e8e1e1  gettext-preserve-errno.patch
cce2f1eeb61e55674469c26871a573cce61d739c3defe9c8f56f2b774f6ba5435849ad542a6714120efddc98c297098e9c98a1a424ac593df2243d4aa479f9a9  ldconfig
378d70e65bcc65bb4e1415354cecfa54b0c1146dfb24474b69e418cdbf7ad730472cd09f6f103e1c99ba6c324c9560bccdf287f5889bbc3ef0bdf0e08da47413  getent.c
9d42d66fb1facce2b85dad919be5be819ee290bd26ca2db00982b2f8e055a0196290a008711cbe2b18ec9eee8d2270e3b3a4692c5a1b807013baa5c2b70a2bbf  iconv.c"
