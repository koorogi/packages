# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kcalutils
pkgver=18.04.3
pkgrel=0
pkgdesc="Utility library for managing a calendar of events"
url="https://www.kde.org/"
arch="all"
options="!check"  # Tests require X11 now
license="LGPL-2.1"
depends=""
depends_dev="qt5-qtbase-dev kcoreaddons-dev kconfig-dev ki18n-dev kcodecs-dev
	kdelibs4support-dev grantlee-dev kcalcore-dev kidentitymanagement-dev"
makedepends="$depends_dev cmake extra-cmake-modules"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kcalutils-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	# Missing template
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E kcalutils-testincidenceformatter
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="8105755af6ee6ff08f072ac58f66ed688bb9c43a6d1947e68234dcbe6e784346b15410fe136d13df822e3ec3f56cef1d1be59dc3ba70a415c1a860f38a0d962a  kcalutils-18.04.3.tar.xz"
