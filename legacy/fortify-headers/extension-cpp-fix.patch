From a29bd89353a05454e2545d52124f9a1a61b4e3e3 Mon Sep 17 00:00:00 2001
From: "A. Wilcox" <AWilcox@Wilcox-Tech.com>
Date: Sat, 23 Jun 2018 17:57:48 -0500
Subject: [PATCH] Don't use __extension__ in C++ code

A few important notes:

*  __extension__ is a GNU C "alternate" keyword, not a C++ keyword.[1]

*  __extension__ is designed to work on "expressions"; it does work on
   #include_next in C mode, but it has no effect in C++ mode; the
   warning will still appear, if enabled, even with __extension__
   preceding #include_next.  This is because #include_next is not
   considered an expression in C++, so the compiler attaches
   __extension__ to the first expression of the header.

All of this leads us to a build failure while building at least all
Mozilla software.  Moz has an alternate -isystem dir searched before
/usr/include that overrides some headers, including <features.h>.  The
first statement in each of these headers is a #pragma, and since
__extension__ is looking for an expression, and #pragma is a "null"
expression, we end up with the following error:

dist/system_wrappers/features.h:1:9: error: '#pragma' is not allowed here

Since __extension__ has no effect on #include_next in C++ mode anyway,
and since it can cause breakage, this commit omits __extension__ in C++
mode.

[1]: https://gcc.gnu.org/onlinedocs/gcc-6.4.0/gcc/Alternate-Keywords.html
---
 include/poll.h       | 2 ++
 include/stdio.h      | 2 ++
 include/stdlib.h     | 4 ++++
 include/string.h     | 2 ++
 include/sys/select.h | 2 ++
 include/sys/socket.h | 2 ++
 include/unistd.h     | 2 ++
 include/wchar.h      | 8 ++++++++
 8 files changed, 24 insertions(+)

diff --git a/include/poll.h b/include/poll.h
index 7b42866..24691f1 100644
--- a/include/poll.h
+++ b/include/poll.h
@@ -16,7 +16,9 @@
 #ifndef _FORTIFY_POLL_H
 #define _FORTIFY_POLL_H
 
+#ifndef __cplusplus
 __extension__
+#endif
 #include_next <poll.h>
 
 #if defined(_FORTIFY_SOURCE) && _FORTIFY_SOURCE > 0 && defined(__OPTIMIZE__) && __OPTIMIZE__ > 0
diff --git a/include/stdio.h b/include/stdio.h
index b67f9ce..a965184 100644
--- a/include/stdio.h
+++ b/include/stdio.h
@@ -16,7 +16,9 @@
 #ifndef _FORTIFY_STDIO_H
 #define _FORTIFY_STDIO_H
 
+#ifndef __cplusplus
 __extension__
+#endif
 #include_next <stdio.h>
 
 #if defined(_FORTIFY_SOURCE) && _FORTIFY_SOURCE > 0 && defined(__OPTIMIZE__) && __OPTIMIZE__ > 0
diff --git a/include/stdlib.h b/include/stdlib.h
index 7ff5746..ef70995 100644
--- a/include/stdlib.h
+++ b/include/stdlib.h
@@ -16,12 +16,16 @@
 #ifndef _FORTIFY_STDLIB_H
 #define _FORTIFY_STDLIB_H
 
+#ifndef __cplusplus
 __extension__
+#endif
 #include_next <stdlib.h>
 
 #if defined(_FORTIFY_SOURCE) && _FORTIFY_SOURCE > 0 && defined(__OPTIMIZE__) && __OPTIMIZE__ > 0
 #if defined(_XOPEN_SOURCE) || defined(_GNU_SOURCE) || defined(_BSD_SOURCE)
+#ifndef __cplusplus
 __extension__
+#endif
 #include_next <limits.h>
 #endif
 
diff --git a/include/string.h b/include/string.h
index ff237b0..43c7485 100644
--- a/include/string.h
+++ b/include/string.h
@@ -16,7 +16,9 @@
 #ifndef _FORTIFY_STRING_H
 #define _FORTIFY_STRING_H
 
+#ifndef __cplusplus
 __extension__
+#endif
 #include_next <string.h>
 
 #if defined(_FORTIFY_SOURCE) && _FORTIFY_SOURCE > 0 && defined(__OPTIMIZE__) && __OPTIMIZE__ > 0
diff --git a/include/sys/select.h b/include/sys/select.h
index e4e398f..bcee8be 100644
--- a/include/sys/select.h
+++ b/include/sys/select.h
@@ -16,7 +16,9 @@
 #ifndef _FORTIFY_SYS_SELECT_H
 #define _FORTIFY_SYS_SELECT_H
 
+#ifndef __cplusplus
 __extension__
+#endif
 #include_next <sys/select.h>
 
 #if defined(_FORTIFY_SOURCE) && _FORTIFY_SOURCE > 0 && defined(__OPTIMIZE__) && __OPTIMIZE__ > 0
diff --git a/include/sys/socket.h b/include/sys/socket.h
index 7d3f023..ad6ab2d 100644
--- a/include/sys/socket.h
+++ b/include/sys/socket.h
@@ -16,7 +16,9 @@
 #ifndef _FORTIFY_SYS_SOCKET_H
 #define _FORTIFY_SYS_SOCKET_H
 
+#ifndef __cplusplus
 __extension__
+#endif
 #include_next <sys/socket.h>
 
 #if defined(_FORTIFY_SOURCE) && _FORTIFY_SOURCE > 0 && defined(__OPTIMIZE__) && __OPTIMIZE__ > 0
diff --git a/include/unistd.h b/include/unistd.h
index d3ab246..71dda84 100644
--- a/include/unistd.h
+++ b/include/unistd.h
@@ -16,7 +16,9 @@
 #ifndef _FORTIFY_UNISTD_H
 #define _FORTIFY_UNISTD_H
 
+#ifndef __cplusplus
 __extension__
+#endif
 #include_next <unistd.h>
 
 #if defined(_FORTIFY_SOURCE) && _FORTIFY_SOURCE > 0 && defined(__OPTIMIZE__) && __OPTIMIZE__ > 0
diff --git a/include/wchar.h b/include/wchar.h
index 7394598..3cb6f92 100644
--- a/include/wchar.h
+++ b/include/wchar.h
@@ -16,13 +16,21 @@
 #ifndef _FORTIFY_WCHAR_H
 #define _FORTIFY_WCHAR_H
 
+#ifndef __cplusplus
 __extension__
+#endif
 #include_next <limits.h>
+#ifndef __cplusplus
 __extension__
+#endif
 #include_next <stdlib.h>
+#ifndef __cplusplus
 __extension__
+#endif
 #include_next <string.h>
+#ifndef __cplusplus
 __extension__
+#endif
 #include_next <wchar.h>
 
 #if defined(_FORTIFY_SOURCE) && _FORTIFY_SOURCE > 0 && defined(__OPTIMIZE__) && __OPTIMIZE__ > 0
-- 
2.15.0

