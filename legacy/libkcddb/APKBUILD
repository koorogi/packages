# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libkcddb
pkgver=18.04.3
pkgrel=0
pkgdesc="Library to retrieve audio CD metadata from the Internet"
url="https://projects.kde.org/projects/kde/kdemultimedia/libkcddb"
arch="all"
license="LGPL-2.0+"
depends=""
depends_dev="qt5-qtbase-dev kconfig-dev kcodecs-dev kio-dev kwidgetsaddons-dev"
makedepends="$depends_dev cmake extra-cmake-modules kdoctools-dev ki18n-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/libkcddb-$pkgver.tar.xz
	update-tests.patch"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="b89db360c2e2abea9a347eb8b3a4dcf9f6d8a4ade689aec7bef7ce674706cb28e078ce73440f320f855b1db80c927c3bd6da71554393a8948ecd5e99537ade8a  libkcddb-18.04.3.tar.xz
81085d4fd1e34a14cdbd436c177904a3e1a4520381e85692abbc3b4126c7bd8b11da63d755ac51dc86dc1345482524cc6f74615afe23fac193c98227dffe0fa8  update-tests.patch"
