=====================================
 README for Adélie Linux Package Set
=====================================
:Authors:
  * **A. Wilcox**, Distro Lead
  * **Elizabeth Myers**, Platform Group
  * **Horst Burkhardt**, Platform Group
  * **William Pitcock**, Platform Group
  * **Adélie Linux Developers and Users**, contributions
:Status:
  Production
:Copyright:
  © 2017-2018 Adélie Linux Team.  NCSA open source licence.




Introduction
============

This repository contains the Adélie Linux package set.  It is used by the
Adélie Linux build system for package building to create the repository used
by Adélie's APK package manager.


Licenses
`````````
As the Adélie Linux project is an open-source Linux distribution, packages
contained in this repository must also be provided under a license recognised
by the OSI_.  No exceptions will be made.

.. _OSI: http://opensource.org/licenses/category


Changes
```````
Any changes to this repository - additions, removal, or version bumps - must
be reviewed before being pushed to the master branch.  There are no exceptions
to this rule.  For security-sensitive updates, contact the Security Team at
sec-bugs@adelielinux.org.




Contents
========

This section contains a high-level view of the contents of this repository.
It does not list every package available; it is merely a guide to help you
find what you need.


``system``: System-level packages
`````````````````````````````````
The ``system`` directory contains packages used by Adélie for core system
functionality.  Special care should be taken before bumping these packages.


``user``: User packages
```````````````````````
The ``user`` directory contains packages that a user would typically be
interested in installing.  Desktop applications, server software (also known as
*daemons*), and other useful packages can be found here.


``legacy``: Orphaned, unmaintained, or deprecated packages
``````````````````````````````````````````````````````````
The ``legacy`` directory contains packages that were at one time supported by
Adélie, but are no longer.  This could be because upstream no longer maintains
the software, its Adélie maintainer has dropped it and nobody has taken it, or
it has been deprecated or replaced with something else.

Use of packages in ``legacy`` are highly discouraged, and packages may be
removed at any time.


``experimental``: Unstable packages
```````````````````````````````````
The ``experimental`` directory contains packages that are highly unstable or
are not yet fully ported to Adélie.  They may not build properly, fail tests,
or exhibit runtime failures.

Please note that a package appearing in ``experimental`` does not denote that
the package will ever be available for Adélie Linux.

Use of packages in ``experimental`` is at your own risk.




Usage
=====

This section contains usage information for this repository.


As an overlay
`````````````

The ``user`` repository can be added as an overlay to any system running APK,
which at the time of this writing includes Alpine Linux, postmarketOS, and a
few others.  However, please ensure you are fully aware of the concerns
surrounding mixing packages in such a manner.  There may be different ABIs or
library versions between each distribution, and packages may crash, experience
instability, or fail to launch at all.  Please be careful.

The domain ``distfiles.adelielinux.org`` is a round-robin for all available
Adélie mirrors.  You may add a repository named above to
``/etc/apk/repositories``:

::

  https://distfiles.adelielinux.org/adelie/$version/$repo

Where ``$version`` is the version of Adélie Linux you are running, or
``stable`` for automatic stable upgrades, or ``current`` for a rolling-release
style distribution (which may be unstable - you have been warned!).

``$repo`` should be replaced with the name of the repository you are wanting
to use, such as ``user``.

Run ``apk update`` to update the package index on your local system.  The
packages will then be available to you.

If you are using the repository on a non-Adélie system, you should use
repository pinning for the Adélie repositories you add.  Please consult the
APK manual for more information about repository pinning.


As a repository
```````````````

The Adélie Linux system is preconfigured to use packages in ``system`` and
``user`` for APK.  No further configuration should be required.

