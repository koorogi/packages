=================================================
 Contribution Guide for Adélie Linux Package Set
=================================================
:Author:
  * **A. Wilcox**, documentation writer
:Status:
  Draft
:Copyright:
  © 2017-2018 Adélie Linux Team.  NCSA open source licence.




Introduction
============

This repository contains the Adélie Linux package set.  It is used by the
Adélie Linux build system for package building to create the repository used
by Adélie's APK package manager.


Licenses
`````````
As the Adélie Linux project is an open-source Linux distribution, packages
contained in this repository must also be provided under a license recognised
by the OSI_.  A separate repository may be provided in the future for certain
non-free components, such as device driver firmware.

.. _OSI: http://opensource.org/licenses/category


Changes
```````
Any changes to this repository - additions, removal, or version bumps - must
be reviewed before being pushed to the master branch.  There are no exceptions
to this rule.  For security-sensitive updates, contact the Security Team at
sec-bugs@adelielinux.org.




Package Format
==============

This section describes the APKBUILD format and patch format required for
packages in this repository.


Source URIs
```````````

It is recommended that any package present in this repository use an official
Adélie Linux mirror for any files downloaded (``mirror://adelie/``), unless
the file resides on the package's official package mirror.

Exceptions will be granted on a case-by-case basis for issues such as file
size or rapid version change.


Comments
````````

It is highly encouraged that you use comments in your package.  This grants
other developers and the Adélie Linux team the same level of understanding
that you have over the package.




Contributing Changes
====================

This section describes the usual flows of contribution to this repository.
For a detailed description of how to contribute to Adélie Linux, review the
Handbook_.

.. _Handbook: https://help.adelielinux.org/html/devel/


GitLab Pull Requests
````````````````````

#. If you do not already have a GitLab account, you must create one.

#. Create a *fork* of the packages repository.  For more information, consult
   the GitLab online documentation.

#. Clone your forked repository to your computer.

#. Make your changes.

#. Test your changes to ensure they are correct.

#. Add (or remove) changed files using ``git add`` and ``git rm``.

#. Commit your changes to the tree using the commands ``git commit -S`` and
   ``git push``.

#. Visit your forked repository in a Web browser.

#. Choose the *Create Pull Request* button.

#. Review your changes to ensure they are correct, and then submit the form.


Mailing List
````````````

#. Clone the packages repository to your computer.

#. Make your changes.

#. Test your changes to ensure they are correct.

#. Add (or remove) changed files using ``git add`` and ``git rm``.

#. Commit your changes to the tree using the command ``git commit -S``.

#. Use the command ``git format-patch HEAD^`` to create a patch file for your
   commit.

   .. note:: If you have made multiple commits to the tree, you will need to
             add an additional ^ for each commit you have made.  For example,
             if you have made three commits, you will use the command
             ``git format-patch HEAD^^^``.

#. Email the resulting patch to the mailing list, using ``git send-email`` or
   your mail client.  The mailing list is adelie-pkg@lists.adelielinux.org.
